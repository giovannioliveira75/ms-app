import { NgModule, ErrorHandler, LOCALE_ID} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FormsModule } from '@angular/forms';

// Pages
import { ActivityPage } from '../pages/activity/activity';
import { ActivityAddPage } from '../pages/activity-add/activity-add';
import { ActivityDetailPage} from '../pages/activity-detail/activity-detail';
import { ActivityUpdatePage } from '../pages/activity-update/activity-update';
import { AuthorizePage} from '../pages/authorize/authorize';
import { ChangeRolePage} from '../pages/change-role/change-role';
import { CondoPage } from '../pages/condo/condo';
import { CondoAddPage } from '../pages/condo-add/condo-add';
import { CondoDetailPage } from '../pages/condo-detail/condo-detail';
import { CondoUpdatePage } from '../pages/condo-update/condo-update';
import { EventPage } from '../pages/event/event';
import { EventAddPage } from '../pages/event-add/event-add';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { EventUpdatePage } from '../pages/event-update/event-update';
import { ExercisePage } from '../pages/exercise/exercise';
import { ExerciseAddPage } from '../pages/exercise-add/exercise-add';
import { ExerciseDetailPage } from '../pages/exercise-detail/exercise-detail';
import { ExerciseUpdatePage } from '../pages/exercise-update/exercise-update';
import { FamiliesPage } from '../pages/families/families';
import { FamiliesUpdatePage } from '../pages/families-update/families-update';
import { GroupPage } from '../pages/group/group';
import { GroupAddPage } from '../pages/group-add/group-add';
import { GroupDetailPage } from '../pages/group-detail/group-detail';
import { GroupUpdatePage } from '../pages/group-update/group-update';
import { GymCardPage } from '../pages/gym-card/gym-card';
import { GymCardAddPage } from '../pages/gym-card-add/gym-card-add';
import { GymCardUpdatePage } from '../pages/gym-card-update/gym-card-update';
import { GymCardHistoryPage } from '../pages/gym-card-history/gym-card-history';
import { GymCardDetailsPage } from '../pages/gym-card-details/gym-card-details';
import { HealthTipAddPage } from '../pages/health-tip-add/health-tip-add';
import { HealthTipDetailPage } from '../pages/health-tip-detail/health-tip-detail';
import { HealthTipUpdatePage } from '../pages/health-tip-update/health-tip-update';
import { HomePage } from '../pages/home/home';
import { InstructorAgendaPage } from '../pages/instructor-agenda/instructor-agenda';
import { InstructorAgendaAddPage} from '../pages/instructor-agenda-add/instructor-agenda-add';
import { InstructorUserAttendancePage } from '../pages/instructor-user-attendance/instructor-user-attendance';
import { NotificationPage } from '../pages/notification/notification';
import { PartnerPage } from '../pages/partner/partner';
import { PartnerAddPage } from '../pages/partner-add/partner-add';
import { PartnerDetailsPage } from '../pages/partner-details/partner-details';
import { PartnerUpdatePage } from '../pages/partner-update/partner-update';
import { ProfilePage } from '../pages/profile/profile';
import { ProfileUpdatePage} from '../pages/profile-update/profile-update';
import { ProgramPage } from '../pages/program/program';
import { ProgramAddPage } from '../pages/program-add/program-add';
import { ProgramDetailsPage } from '../pages/program-details/program-details';
import { ProgramUpdatePage } from '../pages/program-update/program-update';
import { PromotionPage } from '../pages/promotion/promotion';
import { PromotionAddPage } from '../pages/promotion-add/promotion-add';
import { PromotionDetailsPage } from '../pages/promotion-details/promotion-details';
import { PromotionUpdatePage } from '../pages/promotion-update/promotion-update';
import { ReportsPage} from '../pages/reports/reports';
import { ReportsDetailPage} from '../pages/reports-detail/reports-detail';
import { TabsPage } from '../pages/tabs/tabs';
import { UserProfileUpdatePage} from '../pages/user-profile-update/user-profile-update'

//Modules
import { ComponentsModule } from '../components/components.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { LoginPageModule } from '../pages/login/login.module';
import { PopoverPageModule } from '../pages/popover/popover.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { WelcomePageModule } from '../pages/welcome/welcome.module';

// Providers
import { ActivityProvider } from '../providers/activity/activity';
import { AddressProvider } from '../providers/address/address';
import { AlertsProvider } from '../providers/alerts/alerts';
import { AuthProvider } from '../providers/auth/auth';
import { CondoProvider } from '../providers/condo/condo';
import { EventProvider } from '../providers/event/event';
import { ExerciseProvider } from '../providers/exercise/exercise';
import { GroupProvider } from '../providers/group/group';
import { GymCardProvider } from '../providers/gym-card/gym-card';
import { HealthTipProvider } from '../providers/health-tip/health-tip';
import { FamilyProvider } from '../providers/family/family';
import { ImageProvider } from '../providers/image/image';
import { NotificationProvider } from '../providers/notification/notification';
import { PartnerProvider } from '../providers/partner/partner';
import { ProgramProvider } from '../providers/program/program';
import { PromotionProvider } from '../providers/promotion/promotion';
import { RoleProvider } from '../providers/role/role';
import { UserProvider } from '../providers/user/user';

// Firebase
import { firebaseConfig } from '../config';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireModule } from 'angularfire2';

// Natives
import { Camera } from '@ionic-native/camera';
import { Push } from '@ionic-native/push';
import { FCM } from '@ionic-native/fcm';

//CALENDAR
import { NgCalendarModule  } from 'ionic2-calendar';
import { AppointmentProvider } from '../providers/appointment/appointment';

// Language
import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { AttendanceProvider } from '../providers/attendance/attendance';

registerLocaleData(localePtBr);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ActivityPage,
    ActivityAddPage,
    ActivityDetailPage,
    ActivityUpdatePage,
    AuthorizePage,
    ChangeRolePage,
    CondoPage,
    CondoAddPage,
    CondoDetailPage,
    CondoUpdatePage,
    EventPage,
    ExerciseAddPage,
    EventAddPage,
    EventDetailPage,
    EventUpdatePage,
    ExercisePage,
    ExerciseDetailPage,
    ExerciseUpdatePage,
    FamiliesPage,
    FamiliesUpdatePage,
    GroupPage,
    GroupAddPage,
    GroupDetailPage,
    GroupUpdatePage,
    GymCardPage,
    GymCardAddPage,
    GymCardDetailsPage,
    GymCardHistoryPage,
    GymCardUpdatePage,
    HealthTipAddPage,
    HealthTipDetailPage,
    HealthTipUpdatePage,
    InstructorAgendaPage,
    InstructorAgendaAddPage,
    InstructorUserAttendancePage,
    NotificationPage,
    PartnerPage,
    PartnerAddPage,
    PartnerDetailsPage,
    PartnerUpdatePage,
    ProfilePage,
    ProfileUpdatePage,
    ProgramPage,
    ProgramAddPage,
    ProgramDetailsPage,
    ProgramUpdatePage,
    PromotionPage,
    PromotionAddPage,
    PromotionDetailsPage,
    PromotionUpdatePage,
    ReportsPage,
    ReportsDetailPage,
    TabsPage,
    UserProfileUpdatePage
  ],
  imports: [
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages:false
    }),
    WelcomePageModule,
    LoginPageModule,
    SignupPageModule,
    PopoverPageModule,
    ComponentsModule,
    FormsModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig.fire),
    NgCalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ActivityPage,
    ActivityAddPage,
    ActivityDetailPage,
    ActivityUpdatePage,
    AuthorizePage,
    ChangeRolePage,
    CondoPage,
    CondoAddPage,
    CondoDetailPage,
    CondoUpdatePage,
    EventPage,
    EventAddPage,
    EventDetailPage,
    EventUpdatePage,
    ExercisePage,
    ExerciseAddPage,
    ExerciseDetailPage,
    ExerciseUpdatePage,
    FamiliesPage,
    FamiliesUpdatePage,
    GroupPage,
    GroupAddPage,
    GroupDetailPage,
    GroupUpdatePage,
    GymCardPage,
    GymCardAddPage,
    GymCardDetailsPage,
    GymCardHistoryPage,
    GymCardUpdatePage,
    HealthTipAddPage,
    HealthTipDetailPage,
    HealthTipUpdatePage,
    InstructorAgendaPage,
    InstructorAgendaAddPage,
    InstructorUserAttendancePage,
    NotificationPage,
    PartnerPage,
    PartnerAddPage,
    PartnerDetailsPage,
    PartnerUpdatePage,
    ProfilePage,
    ProfileUpdatePage,
    ProgramPage,
    ProgramAddPage,
    ProgramDetailsPage,
    ProgramUpdatePage,
    PromotionPage,
    PromotionAddPage,
    PromotionDetailsPage,
    PromotionUpdatePage,
    ReportsPage,
    ReportsDetailPage,
    TabsPage,
    UserProfileUpdatePage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    ActivityProvider,
    AddressProvider,
    AlertsProvider,
    AngularFireAuth,
    AppointmentProvider,
    AttendanceProvider,
    AuthProvider,
    Camera,
    CondoProvider,
    EventProvider,
    ExerciseProvider,
    FamilyProvider,
    FCM,
    GroupProvider,
    GymCardProvider,
    HealthTipProvider,
    ImageProvider,
    NotificationProvider,    
    PartnerProvider,
    ProgramProvider,
    PromotionProvider,
    Push,
    RoleProvider,
    StatusBar,
    SplashScreen,
    UserProvider
       
  ]
})

export class AppModule {}
