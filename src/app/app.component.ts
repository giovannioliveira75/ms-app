import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WelcomePage } from '../pages/welcome/welcome';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { FCM } from '@ionic-native/fcm';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = WelcomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private push: Push, private alertCtrl: AlertController, private fcm: FCM) {
    platform.ready().then(() => {  

      if (!platform.is('cordova')) {
        console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
        return;
      } else {
        // Notifications       
        fcm.subscribeToTopic('all');
        
        fcm.getToken().then(token=>{
          console.log(token);
        })
        
        fcm.onNotification().subscribe(data=>{
          if(data.wasTapped){
            console.log("Received in background");
          } else {
            console.log("Received in foreground");
            let confirm = alertCtrl.create({
              title: data.title,
              message: data.body
            })
            confirm.present()
          };
        })
        
        fcm.onTokenRefresh().subscribe(token=>{
          console.log(token);
        });
        // end notifications.
      }
    
      
  //     if (!platform.is('cordova')) {
  //       console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
  //       return;
  //   }

  //   const options: PushOptions = {
  //       android: {
  //           senderID: '994580907304' //Just in case it is better to add it.
  //       },
  //       ios: {
  //           alert: 'true',
  //           badge: true,
  //           sound: 'true'
  //       },
  //       windows: {}
  //   };
    
  //   const pushObject: PushObject = this.push.init(options);

  //   pushObject.on('registration').subscribe(data => {
  //       console.log("device token: " + data.registrationId);
  //   });

  //   pushObject.on('notification').subscribe(data => {
  //       console.log(JSON.stringify(data));
  //   });

  //   pushObject.on('error').subscribe((e) => {
  //       console.error(e.message);
  //       console.error(JSON.stringify(e));
  //   });
    });
  }

}