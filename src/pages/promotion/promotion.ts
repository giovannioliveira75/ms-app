import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PromotionDetailsPage } from '../promotion-details/promotion-details';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { PromotionUpdatePage } from '../promotion-update/promotion-update';
import { ImageProvider } from '../../providers/image/image';
import { PromotionAddPage } from '../promotion-add/promotion-add';

/**
 * Generated class for the PromotionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotion',
  templateUrl: 'promotion.html',
})
export class PromotionPage {

  private role: string;
  private promotions: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authProvider: AuthProvider,
              private promotionProvider: PromotionProvider,
              private alertsProvider:AlertsProvider,
              private imageProvider: ImageProvider
            ) {
  }

  ionViewCanEnter(){
    this.promotionProvider.getPromotion()
    .then(res => this.promotions = JSON.parse(JSON.stringify(res)))
    .catch(() => this.alertsProvider.errorAlert())
  }

  ionViewWillEnter() {
    this.role = this.authProvider.userRole;
  }

  create() {
    this.navCtrl.push(PromotionAddPage);
  }
  
  editPromotion(promotion){
    this.navCtrl.push(PromotionUpdatePage, { promotion: promotion})
  }

  showDetails(promotion) {
    this.navCtrl.push(PromotionDetailsPage, {promotion: promotion})
  }

  removePromotion(promotion){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {

      if(!data){
        this.promotionProvider.deletePromotion(promotion.id)
          .then(() => {
            this.imageProvider.removeOld(promotion.image.url)
            this.alertsProvider.successAlert();
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }


}
