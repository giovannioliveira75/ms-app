import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActivityUpdatePage } from '../activity-update/activity-update';
import { CondoProvider } from '../../providers/condo/condo';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the ActivityDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-activity-detail',
  templateUrl: 'activity-detail.html',
})
export class ActivityDetailPage {

  activity: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private condoProvider: CondoProvider, private alertsProvider: AlertsProvider) {
    this.activity = navParams.get('activity');

  }

  editActivity(activity){
    this.navCtrl.push(ActivityUpdatePage, {activity: activity} );
  }

  removeActivity(activity){
    this.condoProvider.deleteCondoActivity(activity).then(( result ) => {
      console.log(result);
      this.alertsProvider.successAlert()
    }, (err) => {
      this.alertsProvider.errorAlert()
    }
  );
  }

}
