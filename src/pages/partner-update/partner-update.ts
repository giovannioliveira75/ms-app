import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImageProvider } from '../../providers/image/image';
import { PartnerProvider } from '../../providers/partner/partner';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the PartnerUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-partner-update',
  templateUrl: 'partner-update.html',
})
export class PartnerUpdatePage {

  private partner:any;
  private updated: boolean;
  private data = {
    "imageUrl": "",
    "name": "",
    "fantasy_name": "",
    "cnpj": "",
    "informations": "",
    "phone": "",
    "email": ""
  };

  private imageUrl:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private imageProvider: ImageProvider,
              private partnerProvider: PartnerProvider,
              private alertsProvider: AlertsProvider
            ) {
    this.partner = navParams.get('partner')
  }

  ionViewWillEnter(){
    this.updated = false;
  }

  ionViewWillLeave(){
    if (this.updated && this.partner.image.url)
      this.imageProvider.removeOld(this.partner.image.url)
    
    this.imageProvider.imageUrl = null
  }

  updatePartner(){
    this.data.name == "" ? this.data.name = this.partner.name : this.data.name;
    this.data.fantasy_name == "" ? this.data.fantasy_name = this.partner.fantasy_name : this.data.fantasy_name;
    this.data.informations == "" ? this.data.informations = this.partner.informations : this.data.informations;
    this.data.phone == "" ? this.data.phone=this.partner.phone : this.data.phone;
    this.data.email == "" ? this.data.email= this.partner.email : this.data.email;
    this.data.cnpj == "" ? this.data.cnpj =this.partner.cnpj : this.data.cnpj;

    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.partner.image.url


    this.partnerProvider.updatePartner(this.data, this.partner.id).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    }, () => {
      this.alertsProvider.errorAlert();
    });
  }

}
