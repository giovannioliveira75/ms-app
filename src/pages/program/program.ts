import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ProgramDetailsPage } from '../program-details/program-details';
import { ProgramProvider } from '../../providers/program/program';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { ProgramUpdatePage } from '../program-update/program-update';
import { UserProvider } from '../../providers/user/user';
import { CondoProvider } from '../../providers/condo/condo';
import { ImageProvider } from '../../providers/image/image';
import { ProgramAddPage } from '../program-add/program-add';

/**
 * Generated class for the ProgramPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-program',
  templateUrl: 'program.html',
})
export class ProgramPage {

  private role: string;
  private programs: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private authProvider: AuthProvider,
              private programProvider:ProgramProvider,
              private alertsProvider:AlertsProvider,
              private userProvider: UserProvider,
              private condoProvider: CondoProvider,
              private imageProvider: ImageProvider
            ) {
  }

  ionViewCanEnter(){
    this.role = this.authProvider.userRole;
    this.getCondoPrograms()
  }

  create() {
    this.navCtrl.push(ProgramAddPage);
  }

  editProgram(program){
    this.navCtrl.push(ProgramUpdatePage, {program: program})
  }

  showDetails(program) {
    this.navCtrl.push(ProgramDetailsPage, { program: program})
  }

  removeProgram(program){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {

      if(!data){
        this.programProvider.deleteProgram(program.id)
          .then(() => {
            this.imageProvider.removeOld(program.image.url)
            this.alertsProvider.successAlert()
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

  getCondoPrograms() {
    this.condoProvider.getCondoPrograms()
    .then(( result ) => {
        this.programs = result;
      })
    .catch(() => {
        this.alertsProvider.errorAlert();
    });
  }

  subscribeProgram(program){
    this.userProvider.addUserPrograms(program)
    .then((res) => {
      if (res === 'Success')
        this.alertsProvider.successSubscribeAlert()
      else if (res === 'Subscribed')
        this.alertsProvider.errorSubscribeAlert()
    })
    .catch(() => this.alertsProvider.errorAlert())

  }

  unsubscribeProgram(program){
    this.userProvider.deleteUserPrograms(program.id)
    .then((res) => {
      if (res === 'Success')
        this.alertsProvider.successUnsubscribeAlert()
      else if (res === 'Unsubscribed')
        this.alertsProvider.errorUnsubscribeAlert()
    })
    .catch(() => this.alertsProvider.errorUnsubscribeAlert())

  }

}
