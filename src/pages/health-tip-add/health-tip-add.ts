import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HealthTipProvider } from '../../providers/health-tip/health-tip';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { ImageProvider } from '../../providers/image/image';
import { AuthProvider } from '../../providers/auth/auth';


/**
 * Generated class for the HealthTipAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-health-tip-add',
  templateUrl: 'health-tip-add.html',
})
export class HealthTipAddPage {

  private isActive: boolean;
  private today :string;
  private imageUrl:any;

  private data = {
      "imageUrl" : "",
      "title" : "",
      "description": "",
      "informations" : "",
      "health_tip_text": "",
      "active": {},
      "email" : ""
    };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private healthTipProvider: HealthTipProvider,
              private alertsProvider: AlertsProvider,
              private imageProvider: ImageProvider,
              private authProvider: AuthProvider
            ) {
  }

  onSelectChange(selectedValue: any) {

  }

  ionViewWillEnter(){
    this.today = new Date().toISOString();
    this.isActive = false;

  }

  addHealthTip(){

    this.imageUrl = this.imageProvider.imageUrl;
    this.data.imageUrl = this.imageUrl;
    this.data.active = this.isActive;
    this.data.email = this.authProvider.userEmail;

    this.healthTipProvider.addHealthTip(this.data)
    .then( () => {
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }

}
