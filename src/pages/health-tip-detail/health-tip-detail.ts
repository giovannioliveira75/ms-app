import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HealthTipDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-health-tip-detail',
  templateUrl: 'health-tip-detail.html',
})
export class HealthTipDetailPage {

  private healthTip:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.healthTip = navParams.get('healthTip');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HealthTipDetailPage');
  }

}
