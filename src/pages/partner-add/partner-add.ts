import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PartnerProvider } from '../../providers/partner/partner';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the PartnerAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-partner-add',
  templateUrl: 'partner-add.html',
})
export class PartnerAddPage {

  private data = {
    "imageUrl": "",
    "name": "",
    "fantasy_name": "",
    "cnpj": "",
    "informations": "",
    "phone": "",
    "email": ""
  };
  private imageUrl: any;
  private created: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private partnerProvider: PartnerProvider,
              private imageProvider: ImageProvider,
              private alertsProvider: AlertsProvider
            ) {
  }

  ionViewWillEnter(){
    this.created = false
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartnerAddPage');
    
  }

  ionViewWillLeave(){
    if (!this.created && this.imageProvider.imageUrl)
      this.imageProvider.removeOld(this.imageProvider.imageUrl)
    this.imageProvider.imageUrl = null
    
  }

  addPartner(){

    this.imageUrl = this.imageProvider.imageUrl;
    this.data.imageUrl = this.imageUrl;

    this.partnerProvider.addPartner(this.data).then( () => {
      this.created = true
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }

}
