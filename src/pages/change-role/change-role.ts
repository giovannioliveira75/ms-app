import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AlertsProvider } from '../../providers/alerts/alerts';



@Component({
  selector: 'page-change-role',
  templateUrl: 'change-role.html',
})
export class ChangeRolePage {

  private users: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProvider: UserProvider,
              private alertsProvider: AlertsProvider,
              private alertCtrl: AlertController

            ) {
  }

  ionViewWillEnter(){
    this.getUsers()
  }

  ionViewDidLoad() {
    console.log("Users", this.users)

  }

  getUsers(){
    this.userProvider.getUsers()
    .then( res => this.users = JSON.parse(JSON.stringify(res)))
    .catch(() => this.alertsProvider.errorAlert())
  }


  alterRole(user) {
    let confirm = this.alertCtrl.create({
      title: 'Mudar perfil',
      message: 'Será atribuido um novo perfil para o usuário selecionado.\nSelecione o novo perfil:',
      inputs: [
        {
          type: 'radio',
          label: 'Administrador',
          value: 'admin'
        },
        {
          type: 'radio',
          label: 'Síndico',
          value: 'sindico'
        },
        {
          type: 'radio',
          label: 'Instrutor',
          value: 'instrutor'
        },
        {
          type: 'radio',
          label: 'Usuário',
          value: 'usuario'
        }

      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Sim',
          handler: (data) => {
            console.log("User", user)
            let newRole = { 'role' : data}
            console.log("User", newRole)
            this.userProvider.updateUserRole(user, newRole)
            .then(res => this.alertsProvider.successAlert())
            .catch(err => this.alertsProvider.errorAlert())


          }
        }
      ]
    });
    confirm.present();
  }

}
