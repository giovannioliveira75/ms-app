import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HealthTipDetailPage } from '../health-tip-detail/health-tip-detail';
import { AuthProvider } from '../../providers/auth/auth';
import { HealthTipProvider } from '../../providers/health-tip/health-tip';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { HealthTipUpdatePage } from '../health-tip-update/health-tip-update';
import { HealthTipAddPage } from '../health-tip-add/health-tip-add';
import { CondoProvider } from '../../providers/condo/condo';
import { Storage } from '@ionic/storage';
import { ImageProvider } from '../../providers/image/image';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private healthTips: any;

  private role;

  constructor(public navCtrl: NavController,
              private authProvider: AuthProvider,
              private healthTipProvider: HealthTipProvider,
              private alertsProvider: AlertsProvider,
              private condoProvider: CondoProvider,
              private storage: Storage,
              private imageProvider: ImageProvider
            ) {
      this.role = this.authProvider.userRole;
  }

  create() {
    this.navCtrl.push(HealthTipAddPage);
  }

  ionViewWillEnter() {
    if (this.role != 'usuario'){
      this.condoProvider.getCondos()
      .then(res => {
        let userCondos = res
        let userCondo = res[0].name
        this.storage.set('userCondos', userCondos)
        this.storage.set('userCondo', userCondo)
      }).catch(err => this.alertsProvider.errorAlert())
    }

    this.getHealthTips();
  }


  ionViewDidLoad(){
    this.getHealthTips();
  }


  getHealthTips(){
    this.healthTipProvider.getHealthTips().then(( result ) => {
      this.healthTips = result;
    });
  }

  showDetails(healthTip){
    this.navCtrl.push(HealthTipDetailPage, { healthTip: healthTip});
  }

  addHealthTip(){
    this.navCtrl.push(HealthTipAddPage)
  }

  editHealthTip(healthTip){
    this.navCtrl.push(HealthTipUpdatePage, {healthTip: healthTip})
  }

  removeHealthTip(healthTip){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {

      if(!data){
        this.healthTipProvider.deleteHealthTip(healthTip.id)
          .then(() => {
            this.imageProvider.removeOld(healthTip.image.url)
            this.alertsProvider.successAlert()
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

}
