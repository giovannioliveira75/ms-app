import { EventDetailPage } from './../event-detail/event-detail';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EventProvider } from '../../providers/event/event';
import { AuthProvider } from '../../providers/auth/auth';
import { EventUpdatePage } from '../event-update/event-update';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { Storage } from '@ionic/storage';
import { CondoProvider } from '../../providers/condo/condo';
import { UserProvider } from '../../providers/user/user';
import { ImageProvider } from '../../providers/image/image';
import { EventAddPage } from '../event-add/event-add';


@Component({
  selector: 'page-event',
  templateUrl: 'event.html'
})
export class EventPage {

  private events: any;
  private role;
  private condo: any;
  private condoEvents: any;

  constructor(public navCtrl: NavController,
              private storage: Storage,
              private eventProvider: EventProvider,
              private authProviser: AuthProvider,
              private alertsProvider: AlertsProvider,
              private condoProvider: CondoProvider,
              private userProvider: UserProvider,
              private imageProvider: ImageProvider
            ) {

  }

  create() {
    this.navCtrl.push(EventAddPage);
  }

  ionViewCanEnter() {
    this.storage.get('userCondo').then( (value) => {
      this.condo = value;
    });
  }

  ionViewWillEnter() {
    this.role = this.authProviser.userRole;
    if (this.role == "usuario" || this.role == "sindico") {
      // this.getPublicEvents();
      this.getCondoEvents();
    } else this.getEvents();
  }

  ionViewDidLoad () {

  }

  editEvent(event){
    this.navCtrl.push(EventUpdatePage, { event: event})
  }

  showDetails(event) {
    this.navCtrl.push(EventDetailPage, {event: event})
  }

  removeEvent(event){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.eventProvider.deleteEvent(event.id)
          .then(() => {
            this.imageProvider.removeOld(event.image.url)
            this.alertsProvider.successAlert()
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

  selectEventToAdd(){

  }

  subscribeEvent(event){
    this.userProvider.addUserEvents(event)
    .then((res) => {
      if (res === 'Success')
        this.alertsProvider.successSubscribeAlert()
      else if (res === 'Subscribed')
        this.alertsProvider.errorSubscribeAlert()
    })
    .catch(() => this.alertsProvider.errorAlert())

  }

  unsubscribeEvent(event){
    this.userProvider.deleteUserEvents(event.id)
    .then((res) => {
      if (res === 'Success')
        this.alertsProvider.successUnsubscribeAlert()
      else if (res === 'Unsubscribed')
        this.alertsProvider.errorUnsubscribeAlert()
    })
    .catch(() => this.alertsProvider.errorUnsubscribeAlert())

  }

  getEvents () {
    this.eventProvider.getEvents().then(
      result => {
        this.events = JSON.parse(JSON.stringify(result));
      }).catch(err => this.alertsProvider.errorAlert())
  }

  getPublicEvents(){
    this.eventProvider.getPublicEvents().then(
      result => {
        this.events = JSON.parse(JSON.stringify(result));
      }).catch(err => this.alertsProvider.errorAlert())
  }

  getCondoEvents () {
    this.condoProvider.getCondoEvents().then(
      result => {
        this.condoEvents = JSON.parse(JSON.stringify(result));
      }).catch(err => this.alertsProvider.errorAlert())
  }

}
