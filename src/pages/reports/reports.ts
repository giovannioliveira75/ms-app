import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { AttendanceProvider } from '../../providers/attendance/attendance';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { UserProvider } from '../../providers/user/user';
import { CondoProvider } from '../../providers/condo/condo';
import { ReportsDetailPage } from '../reports-detail/reports-detail';

/**
 * Generated class for the ReportsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  private role:string;
  private search: boolean;
  private hasInsctructor: boolean;
  private hasCondo: boolean;
  private start_date: string;
  private end_date: string;
  private reportTypes = [];
  private reportType: string;

  private attendances:any;
  private instructors:any;
  private instructor:any;
  private condos:any;
  private condo:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public app: App,
              private authProvider: AuthProvider,
              private attendaceProvider: AttendanceProvider,
              private alertsProvider: AlertsProvider,
              private userProvider: UserProvider,
              private condoProvider: CondoProvider
            ) {
  }

  ionViewCanEnter(){
    this.userProvider.getInstructors()
    .then( res => this.instructors = JSON.parse(JSON.stringify(res) ))
    .catch( () => this.alertsProvider.errorAlert())

    this.condoProvider.getCondos()
    .then( res => this.condos = JSON.parse(JSON.stringify(res) ))
    .catch( () => this.alertsProvider.errorAlert())
  }

  ionViewWillEnter() {
    this.role = this.authProvider.userRole;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportsPage');
    this.search = false
    this.hasInsctructor = false
    this.hasCondo = false
    this.start_date = ""
    this.end_date = ""
    this.reportTypes = ['Frequência de instrutores',
                        'Frequência de alunos',
                        'Condomínios',
                        'Atividades',
                        'Atividades por condomínio',
                        'Eventos',
                        'Eventos por condomínio',
                        'Programas',
                        'Programas por condomínio',
                        'Agenda',
                        'Agenda por instrutor'
                      ]
    this.reportType = ""
  }

  onSelectChangeReportTypes(SelectedValue:any){
    this.reportType = SelectedValue
  }

  onSelectChangeInstructors(SelectedValue:any){
    this.instructor = SelectedValue
  }

  onSelectChangeCondos(SelectedValue:any){
    this.condo = SelectedValue
  }

  getAttendances(){
    this.attendaceProvider.getAttendances()
    .then((res) => this.attendances = JSON.parse(JSON.stringify(res)))
    .catch(() => this.alertsProvider.errorAlert())
  }

  searchAttendances(reportType){
    switch (reportType) {
      case 'Frequência de instrutores':
        this.searchAttendancesByInstructor()
        break;
      case 'Frequência de alunos':
        this.searchAttendancesByUser()
        break;
      default:
        this.alertsProvider.notAvailableAlert()
        break;
    }
  }

  searchAttendancesByInstructor(){
    this.attendaceProvider.getAttendancesByInstructor(this.start_date, this.end_date, this.instructor)
    .then((res) => this.navCtrl.push(ReportsDetailPage, {attendances : JSON.parse(JSON.stringify(res))}))
    .catch(()=> this.alertsProvider.errorAlert())
  }

  searchAttendancesByUser(){
    this.attendaceProvider.getAttendancesByUser(this.start_date, this.end_date, this.instructor)
    .then((res) => this.navCtrl.push(ReportsDetailPage, {attendances : JSON.parse(JSON.stringify(res))}))
    .catch(()=> this.alertsProvider.errorAlert())
  }



}
