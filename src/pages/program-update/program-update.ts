import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImageProvider } from '../../providers/image/image';
import { ProgramProvider } from '../../providers/program/program';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the ProgramUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-program-update',
  templateUrl: 'program-update.html',
})
export class ProgramUpdatePage {

  private program:any;
  private updated: boolean;
  today:string;
  private imageUrl:any;

  private data = {
    "imageUrl" : "",
    "title" : "",
    "description": "",
    "informations" : "",
    "program_text": "",
    "start_date" : "",
    "end_date" : "",
    "max_users": "",
    "min_users": "",
    "condos": { },
    "instructors": { },
    "active": {},
    "private": {}

  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private imageProvider: ImageProvider,
              private programProvider:ProgramProvider,
              private alertsProvider: AlertsProvider
            ) {
    this.program = navParams.get('program')
  }

  ionViewWillEnter(){
    this.today = new Date().toISOString();
    this.updated = false;
  }

  ionViewWillLeave(){
    if (this.updated && this.program.image.url)
      this.imageProvider.removeOld(this.program.image.url)
    // else if(this.imageProvider.imageUrl && !this.updated)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
  }


  updateProgram(){
    this.data.title == "" ? this.data.title = this.program.title : this.data.title;
    this.data.description == "" ? this.data.description = this.program.description : this.data.description;
    this.data.informations == "" ? this.data.informations = this.program.informations : this.data.informations;
    this.data.program_text == "" ? this.data.program_text=this.program.program_text : this.data.program_text;
    this.data.start_date == "" ? this.data.start_date= this.program.start_date : this.data.start_date;
    this.data.end_date == "" ? this.data.end_date =this.program.end_date : this.data.end_date;

    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.program.image.url


    this.programProvider.updateProgram(this.data, this.program.id).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    }, () => {
      this.alertsProvider.errorAlert();
    });
  }

}
