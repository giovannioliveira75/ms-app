import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ExerciseAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exercise-add',
  templateUrl: 'exercise-add.html',
})
export class ExerciseAddPage {

  private isActive: boolean;
  private groups: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter(){
    this.isActive = false
    this.groups = {}
  }

  addExercise(){

  }

}
