import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { ImageProvider } from '../../providers/image/image';


@Component({
  selector: 'page-user-profile-update',
  templateUrl: 'user-profile-update.html',
})
export class UserProfileUpdatePage {

  private condoChanged:boolean;
  private condos: any;
  private newCondo:any;
  private role:any
  private updated:boolean;
  private user:any;
  private imageUrl:any;

  private data = {
    "social_number": "",
    "birth_date":"",
    "height": "",
    "weight": "",
    "emergency_name": "",
    "emergency_phone" : "",
    "observations": "",
    "building":"",
    "apartment":"",
    "imageUrl": ""
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProvider: UserProvider,
              private authProvider: AuthProvider,
              private alertsProvider: AlertsProvider,
              private imageProvider: ImageProvider
              
            ) {
    this.user = navParams.get('user')
  }

  ionViewCanEnter(){
    this.role = this.authProvider.userRole
  }

  ionViewWillEnter(){
    this.updated = false

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserProfileUpdatePage');
    console.log("User: ", this.user)
  }

  ionViewWillLeave(){
    if (this.updated && this.user.profileImage)
      this.imageProvider.removeOld(this.user.profileImage)
    // else if(this.imageProvider.imageUrl && !this.updated)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
    this.imageProvider.imageUrl = null
  }


  updateUserProfile(){
    this.data.social_number == "" ? this.data.social_number = this.user.social_number : this.data.social_number;
    this.data.birth_date == "" ? this.data.birth_date = this.user.birth_date : this.data.birth_date;
    this.data.height == "" ? this.data.height = this.user.height : this.data.height;
    this.data.weight == "" ? this.data.weight=this.user.weight : this.data.weight;
    this.data.emergency_name == "" ? this.data.emergency_name= this.user.emergency_name : this.data.emergency_name;
    this.data.emergency_phone == "" ? this.data.emergency_phone = this.user.emergency_phone : this.data.emergency_phone;
    this.data.observations == "" ? this.data.observations = this.user.observations : this.data.observations;
    this.data.building == "" ? this.data.building = this.user.building : this.data.building;
    this.data.apartment == "" ? this.data.apartment = this.user.apartment : this.data.apartment;

    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.user.profileImage

    this.userProvider.updateUser(this.data).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    })
    .catch( () => {
      this.alertsProvider.errorAlert();
    });
    
  }

}
