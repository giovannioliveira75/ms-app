import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { TabsPage } from '../tabs/tabs';
import { AuthProvider } from '../../providers/auth/auth';
import { Storage } from '@ionic/storage';
import { ProfileUpdatePage } from '../profile-update/profile-update';
import { AlertsProvider } from '../../providers/alerts/alerts';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private email: string;
  private password: string;
  private loading: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              private authProvider: AuthProvider,
              private alertsProvider: AlertsProvider,
              private storage: Storage
            ) {
  }

  ionViewCanEnter(){

  }

  ionViewDidLoad() {
    this.presentLoading();
    this.authProvider.checkAuth().then((res) => {
      this.loading.dismiss();
      this.checkProfileStatus();
    }, ( err ) => {
      this.loading.dismiss();
    });
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Realizando autenticação...'
    });

    this.loading.present();

  }

  checkProfileStatus(){
    if (this.authProvider.userRole == "usuario") {
      this.storage.get('profileUpdated').then( (value) => {
        (value == true ) ? this.checkAuthorization() : this.navCtrl.setRoot(ProfileUpdatePage);
      })
    } else this.navCtrl.setRoot(TabsPage)
  }

  checkAuthorization (){
    if (this.authProvider.userRole == "usuario") {
      this.storage.get('authorized').then((value) => {
        (value)  ? this.navCtrl.setRoot(TabsPage) : this.navCtrl.setRoot(ProfileUpdatePage) ;

      });
    } else this.navCtrl.setRoot(TabsPage)

  }

  logIn(){
    this.presentLoading();

    let credentials = {
      username: this.email,
      password: this.password,
    };


    this.authProvider.login(credentials)
      .then(result => {
        this.loading.dismiss();
        this.checkAuthorization();
      }, (err) => {
        this.alertsProvider.loginErrorAlert();
        this.loading.dismiss();
      });
  }

  signup(){
    this.navCtrl.push(SignupPage)
  }


}
