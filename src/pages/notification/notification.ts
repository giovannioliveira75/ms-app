import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification/notification';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  private notificationTitle: string;
  private notificationText: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private notification: NotificationProvider, private alerts: AlertsProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  sendNotification(){
    this.notification.sendNotification(this.notificationTitle, this.notificationText)
    .then( () => {
      this.notificationText = ""
      this.notificationTitle = ""
    })
    .catch( () => this.alerts.errorAlert())
    
  }

}
