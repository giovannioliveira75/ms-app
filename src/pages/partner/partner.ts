import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PartnerDetailsPage } from '../partner-details/partner-details';
import { PartnerProvider } from '../../providers/partner/partner';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { PartnerUpdatePage } from '../partner-update/partner-update';
import { ImageProvider } from '../../providers/image/image';
import { PartnerAddPage } from '../partner-add/partner-add';

/**
 * Generated class for the PartnerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-partner',
  templateUrl: 'partner.html',
})
export class PartnerPage {

  private role: string;
  private partners:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authProvider: AuthProvider,
              private partnerProvider: PartnerProvider,
              private alertsProvider: AlertsProvider,
              private imageProvider: ImageProvider
            ) {

  }

  ionViewCanEnter(){    
    this.partnerProvider.getPartners()
    .then(res => this.partners = JSON.parse(JSON.stringify(res)))
    .catch(() => this.alertsProvider.errorAlert())
  }

  ionViewWillEnter() {
    this.role = this.authProvider.userRole;
  }

  create() {
    this.navCtrl.push(PartnerAddPage)
  }

  editPartner(partner){
    this.navCtrl.push(PartnerUpdatePage, {partner: partner})
  }

  showDetails(partner) {
    this.navCtrl.push(PartnerDetailsPage, { partner: partner})
  }

  removePartner(partner){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {

      if(!data){
        this.partnerProvider.deletePartner(partner.id)
          .then(() => {
            this.imageProvider.removeOld(partner.image.url)
            this.alertsProvider.successAlert();
            this.navCtrl.popToRoot()
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

}
