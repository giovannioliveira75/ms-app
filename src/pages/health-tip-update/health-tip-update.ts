import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HealthTipProvider } from '../../providers/health-tip/health-tip';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';


@Component({
  selector: 'page-health-tip-update',
  templateUrl: 'health-tip-update.html',
})
export class HealthTipUpdatePage {

  private healthTip: any;
  private isActive: boolean;
  private updated: boolean;
  private today: string;
  private imageUrl: any;

  private data = {
    "imageUrl": "",
    "title": "",
    "description": "",
    "informations": "",
    "health_tip_text": "",
    "active": {}
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private healthTipProvider: HealthTipProvider,
    private imageProvider: ImageProvider,
    private alertsProvider: AlertsProvider
  ) {
    this.healthTip = navParams.get('healthTip')
  }

  ionViewWillEnter() {
    this.today = new Date().toISOString();
    this.isActive = false;
    this.data = {
      "imageUrl": "",
      "title": this.healthTip.title,
      "description": this.healthTip.description,
      "informations": this.healthTip.informations,
      "health_tip_text": this.healthTip.health_tip_text,
      "active": {}
    };
    this.updated = false;
  }

  ionViewWillLeave(){
    if (this.updated && this.healthTip.image.url)
      this.imageProvider.removeOld(this.healthTip.image.url)
    // else if(this.imageProvider.imageUrl && !this.updated)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
  }

   updateHealthTip() {

    this.data.title == this.healthTip.title ? this.data.title = this.healthTip.title : this.data.title;
    this.data.description == this.healthTip.description ? this.data.description = this.healthTip.description : this.data.description;
    this.data.informations == this.healthTip.informations ? this.data.informations = this.healthTip.informations : this.data.informations;
    this.data.health_tip_text == this.healthTip.health_tip_text ? this.data.health_tip_text = this.healthTip.health_tip_text : this.data.health_tip_text;
    this.data.active = this.isActive;

    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.healthTip.image.url

    this.healthTipProvider.updateHealthTip(this.data, this.healthTip.id)
    .then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    })
    .catch( () => {
      this.alertsProvider.errorAlert();
    });
  }

}
