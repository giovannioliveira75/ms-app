import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ExerciseProvider } from '../../providers/exercise/exercise';

/**
 * Generated class for the ExerciseDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exercise-detail',
  templateUrl: 'exercise-detail.html',
})
export class ExerciseDetailPage {

  private exercise: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public exerciseProvider: ExerciseProvider) {
    this.exercise = navParams.get('exercise');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExerciseDetailPage');
  }

}
