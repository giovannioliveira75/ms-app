import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ActivityDetailPage } from '../activity-detail/activity-detail';
import { CondoProvider } from '../../providers/condo/condo';
import { Storage } from '@ionic/storage';
import { ActivityProvider } from '../../providers/activity/activity';
import { CondoPage } from '../condo/condo';
import { AuthProvider } from '../../providers/auth/auth';
import { ActivityUpdatePage } from '../activity-update/activity-update';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { UserProvider } from '../../providers/user/user';
import { ImageProvider } from '../../providers/image/image';
import { ActivityAddPage } from '../activity-add/activity-add';



@Component({
  selector: 'page-activity',
  templateUrl: 'activity.html'
})
export class ActivityPage {

  private activities:any;
  private role: string;
  private condo:any;



  constructor(public navCtrl: NavController,
              public storage: Storage,
              private condoProvider: CondoProvider,
              private activityProvider: ActivityProvider,
              private authProvider: AuthProvider,
              private alertsProvider: AlertsProvider,
              private userProvider: UserProvider,
              private imageProvider: ImageProvider
  ) {

  }

  ionViewWillEnter() {
    this.role = this.authProvider.userRole;
    // this.role == "usuario" || this.role == "sindico" ? this.getCondoActivities() : this.getActivities() ;
    this.getCondoActivities()
    this.condo = this.authProvider.userCondo;
  }

  ionViewDidEnter(){
    this.getCondoActivities()
  }

  create() {
    this.navCtrl.push(ActivityAddPage);
  }


  getActivities(){
    this.activityProvider.getActivities().then(( result ) => {
      this.activities = result;
    }, (err) => {
      console.log(err);
    });
  }

  getCondoActivities() {
    this.condoProvider.getCondoActivities().then(( result ) => {
        this.activities = result;
      }, (err) => {
        this.alertsProvider.errorAlert();
      }
    );
  }

  showDetails(activity){
    this.navCtrl.push(ActivityDetailPage, {activity: activity} );
  }

  editActivity(activity){
    this.navCtrl.push(ActivityUpdatePage, {activity: activity})
  }

  removeActivity(activity){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.activityProvider.deleteActivity(activity.id)
          .then(() => {
            this.imageProvider.removeOld(activity.image.url)
            this.alertsProvider.successAlert()
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

  subscribeActivity(activity){
    this.userProvider.addUserActivities(activity)
    .then((res) => {
      if (res === 'Success')
        this.alertsProvider.successSubscribeAlert()
      else if (res === 'Subscribed')
        this.alertsProvider.errorSubscribeAlert()
    })
    .catch(() => this.alertsProvider.errorAlert())

  }

  unsubscribeActivity(activity){
    this.userProvider.deleteUserActivities(activity.id)
    .then((res) => {
      if (res === 'Success')
        this.alertsProvider.successUnsubscribeAlert()
      else if (res === 'Unsubscribed')
        this.alertsProvider.errorUnsubscribeAlert()
    })
    .catch(() => this.alertsProvider.errorUnsubscribeAlert())

  }

  switchCondo() {
    this.navCtrl.push(CondoPage);
  }

}
