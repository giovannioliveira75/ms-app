import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { CondoProvider } from '../../providers/condo/condo';
import { ImageProvider } from '../../providers/image/image';
import { ProgramProvider } from '../../providers/program/program';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the ProgramAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-program-add',
  templateUrl: 'program-add.html',
})
export class ProgramAddPage {

  private hasCondo: boolean;
  private hasInstructor: boolean;
  private isActive: boolean;
  private today: string;
  private imageUrl:any;
  private condos = {}
  private instructors = {}

  private data = {
    "imageUrl" : "",
    "title" : "",
    "description": "",
    "informations" : "",
    "program_text": "",
    "start_date" : "",
    "end_date" : "",
    "max_users": "",
    "min_users": "",
    "private": {},
    "condos": { },
    "instructors": { },
    "active": {}
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userProvider: UserProvider,
              private condoProvider: CondoProvider,
              private imageProvider : ImageProvider,
              private programProvider:ProgramProvider,
              private alertsProvider: AlertsProvider

            ) {
  }

  ionViewCanEnter() {
    this.getCondos()
    this.getInstructors()
  }

  ionViewWillEnter(){
    this.today = new Date().toISOString();
    this.hasCondo = false
    this.isActive = false
    this.hasInstructor = false
  }

  getInstructors(){
    this.userProvider.getInstructors()
    .then( instructors => this.instructors = instructors)
    .catch(err => this.alertsProvider.errorAlert())
  }

  getCondos(){
    this.condoProvider.getCondos().then( res => {
      this.condos = res;
    }).catch(err => this.alertsProvider.errorAlert())
  }


  onSelectCondoChange(selectedValue: any) {
    this.data.condos = selectedValue;
  }

  onSelectInstructorChange(selectedValue: any) {
    this.data.instructors = selectedValue;
  }

  addProgram(){
    this.imageUrl = this.imageProvider.imageUrl;
    this.data.imageUrl = this.imageUrl;
    this.isActive ? this.data.active = true : this.data.active = false;
    this.hasCondo ? this.data.private = true : this.data.private = false;

    this.programProvider.addProgram(this.data)
    .then( () => {
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }

}
