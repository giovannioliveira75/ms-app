import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { PartnerProvider } from '../../providers/partner/partner';
import { ImageProvider } from '../../providers/image/image';
import { PartnerUpdatePage } from '../partner-update/partner-update';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the PartnerDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-partner-details',
  templateUrl: 'partner-details.html',
})
export class PartnerDetailsPage {

  private partner: any;
  private role:string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertsProvider: AlertsProvider,
              private partnerProvider: PartnerProvider,
              private imageProvider: ImageProvider,
              private authProvider: AuthProvider
            ) {
    this.partner = navParams.get('partner')
  }

  ionViewWillEnter() {
    this.role = this.authProvider.userRole
  }

  editPartner(partner){
    this.navCtrl.push(PartnerUpdatePage, {partner: partner})
  }

  removePartner(partner){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {

      if(!data){
        this.partnerProvider.deletePartner(partner.id)
          .then(() => {
            this.imageProvider.removeOld(partner.image.url)
            this.alertsProvider.successAlert();
            this.navCtrl.popToRoot()
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

}
