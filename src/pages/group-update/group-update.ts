import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GroupUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-group-update',
  templateUrl: 'group-update.html',
})
export class GroupUpdatePage {

  private group: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.group = navParams.get('group')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroupUpdatePage');
  }

}
