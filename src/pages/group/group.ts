import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GroupDetailPage } from '../group-detail/group-detail';
import { GroupProvider } from '../../providers/group/group';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { GroupUpdatePage } from '../group-update/group-update';

/**
 * Generated class for the GroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
})
export class GroupPage {

  private role;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authProvider: AuthProvider,
              private groupsProvider: GroupProvider,
              private alertsProvider: AlertsProvider
            ) {
  }

  ionViewWillEnter() {
    this.role = this.authProvider.userRole;
  }


  editGroup(group){
    this.navCtrl.push(GroupUpdatePage, {group : group})
  }

  showDetails(group) {
    this.navCtrl.push(GroupDetailPage, {group:group})
  }

  removeGroup(group){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.groupsProvider.deleteGroup(group.id)
          .then(() => {
            this.alertsProvider.successAlert();
            this.navCtrl.setRoot(GroupPage)
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

  selectGroupToAdd(){

  }
}
