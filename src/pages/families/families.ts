import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FamilyProvider } from '../../providers/family/family';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { FamiliesUpdatePage } from '../families-update/families-update';

/**
 * Generated class for the FamiliesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-families',
  templateUrl: 'families.html',
})
export class FamiliesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private familyProvider: FamilyProvider, private alertsProvider:AlertsProvider) {
  }

  editFamilies(family){
    this.navCtrl.push(FamiliesUpdatePage, { family: family})
  }

  removeFamily(family) {
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.familyProvider.deleteFamily(family.id)
          .then(() => {
            this.alertsProvider.successAlert();
            this.navCtrl.setRoot(FamiliesPage)
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }
}
