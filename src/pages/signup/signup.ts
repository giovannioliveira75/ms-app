import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';


/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  responseMsg = "";
  userData = { "name": "", "email": "", "password": "", "confPassword": ""};
  loading: any;
  toast: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public authProvider: AuthProvider) {
  }

  ionViewDidLoad() {

    this.presentLoading();
    this.authProvider.checkAuth().then((res) => {
      this.loading.dismiss();
      this.navCtrl.push(TabsPage);
    }, ( err ) => {
      this.loading.dismiss();

    });
  }



  login(){
    this.navCtrl.push(LoginPage);
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Registro Feito com Sucesso',
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  errorToast() {
    this.toast = this.toastCtrl.create({
      message: this.responseMsg,
      showCloseButton: true,
      closeButtonText: 'Ok',
      position: 'top',
    });
    this.toast.present();
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Loading Please Wait...'

    });

    this.loading.present();

  }

  signUp() {

    if ((this.checkUsername(this.userData.name) && this.checkEmail(this.userData.email)) && this.checkPassword(this.userData.password,this.userData.confPassword)) {

      this.authProvider.signUp(this.userData).then((result) => {
        console.log(result);
        this.presentToast();
        this.navCtrl.push(LoginPage);
      }, (err) => {
        console.log(err.status);
      });
    } else {
      this.errorToast();
    }
  }

  checkPassword(password, confPassword) {
    if ((password == null || password == "") || (confPassword == null || confPassword == "") ) {
        this.responseMsg += "Campos de senha obrigatórios.\n\n";
        return false;
    } else if ( password != confPassword){
      this.responseMsg += "As senhas não são as mesmas.\n\n";
      return false;
    } else {
      return true;
    }

  }

  checkUsername(name) {
    if (name == null || name == ""){
      this.responseMsg += "Campo 'nome' é obrigatório.\n\n";
      return false;
    } else {
       return true;
    }
  }

  checkEmail(email) {
    if (email == null || email == ""){
      this.responseMsg += "Campo 'email' é obrigatório.\n\n";
      return false;
    } else {
      return true;
    }

  }

  // checkEmailValidation(email) {
  //   if (email.includes('@')){
  //     return true;
  //   } else {
  //     this.responseMsg += "Digite um email válido.\n\n";
  //     return false;
  //   }
  // }

}
