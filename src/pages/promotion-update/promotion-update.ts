import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImageProvider } from '../../providers/image/image';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the PromotionUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotion-update',
  templateUrl: 'promotion-update.html',
})
export class PromotionUpdatePage {

  private promotion:any;

  private data = {
    "imageUrl": "",
    "title": "",
    "description" : "",
    "promotion_text": "",
    "start_date": "",
    "end_date": "",
    "active": {},
    "partner": ""
  }

  private today:string;
  private active:boolean;
  private imageUrl:any;
  private updated: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private imageProvider: ImageProvider,
              private promotionProvider: PromotionProvider,
              private alertsProvider: AlertsProvider
            ) {
    this.promotion = navParams.get('promotion')
  }

  ionViewWillEnter(){
    this.today = new Date().toISOString();
    this.updated = false
    this.active = false
  }

  ionViewWillLeave(){
    if (this.updated && this.promotion.image.url)
      this.imageProvider.removeOld(this.promotion.image.url)
    
    this.imageProvider.imageUrl = null
  }

  updatePromotion(){
    this.data.title == "" ? this.data.title = this.promotion.title : this.data.title;
    this.data.description == "" ? this.data.description = this.promotion.description : this.data.description;
    this.data.promotion_text == "" ? this.data.promotion_text=this.promotion.promotion_text : this.data.promotion_text;
    this.data.start_date == "" ? this.data.start_date= this.promotion.start_date : this.data.start_date;
    this.data.end_date == "" ? this.data.end_date =this.promotion.end_date : this.data.end_date;
    this.data.active ? this.data.active = this.promotion.active : this.data.active;
    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.promotion.image.url


    this.promotionProvider.updatePromotion(this.data, this.promotion.id).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    }, () => {
      this.alertsProvider.errorAlert();
    });
  }

}
