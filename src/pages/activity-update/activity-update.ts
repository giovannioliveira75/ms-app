import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActivityProvider } from '../../providers/activity/activity';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';


@Component({
  selector: 'page-activity-update',
  templateUrl: 'activity-update.html',
})
export class ActivityUpdatePage {

  private updated: boolean;
  activity: any;
  today:string;
  imageUrl:any;
  data = { "imageUrl": "", "title": "", "description": "", "informations": "", "activity_text": "", "start_date": "", "end_date": ""};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private activityProvider: ActivityProvider,
    private imageProvider: ImageProvider,
    private alertsProvider: AlertsProvider

  ) {
    this.activity = navParams.get('activity');
  }

  ionViewWillEnter(){
    this.today = new Date().toISOString();
    this.updated = false;
  }

  ionViewWillLeave(){
    if (this.updated && this.activity.image.url)
      this.imageProvider.removeOld(this.activity.image.url)
    // else if(this.imageProvider.imageUrl && !this.updated)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
    this.imageProvider.imageUrl = null
  }

  updateActivity(){
    this.data.title == "" ? this.data.title = this.activity.title : this.data.title;
    this.data.description == "" ? this.data.description = this.activity.description : this.data.description;
    this.data.informations == "" ? this.data.informations = this.activity.informations : this.data.informations;
    this.data.activity_text == "" ? this.data.activity_text=this.activity.activity_text : this.data.activity_text;
    this.data.start_date == "" ? this.data.start_date= this.activity.start_date : this.data.start_date;
    this.data.end_date == "" ? this.data.end_date =this.activity.end_date : this.data.end_date;

    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.activity.image.url


    this.activityProvider.updateActivity(this.data, this.activity.id).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    }, () => {
      this.alertsProvider.errorAlert();
    });
  }



}
