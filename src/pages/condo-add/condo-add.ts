import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { CondoProvider } from '../../providers/condo/condo';
import { HttpClient } from '@angular/common/http';
import { AuthProvider } from '../../providers/auth/auth';


@Component({
  selector: 'page-condo-add',
  templateUrl: 'condo-add.html',
})
export class CondoAddPage {

  private cities = []
  private states = []
  private data = {
    "name": "",
    "condo_info" : "",
    "street": "",
    "number": "",
    "postal_code": "",
    "complement": "",
    "state": "",
    "city": "",
    "imageUrl": ""
  }
  private stateSelected:boolean = false;
  private selectedState: any;
  private selectedCity: any;
  private imageUrl:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private imageProvider: ImageProvider,
              private alertsProvider: AlertsProvider,
              private condoProvider: CondoProvider,
              private http: HttpClient,
              private authProvider:AuthProvider
            ) {
  }

  ionViewCanEnter(){
    this.getStates()
  }

  getStates(){
    this.http.get(this.authProvider.apiUrl+'states', {headers: this.authProvider.appendHeaders()})
    .subscribe( data => {
      this.states = JSON.parse(JSON.stringify(data));
    })
  }

  getCities() {
    this.http.get(this.authProvider.apiUrl+'states/'+ this.selectedState.abbreviation + '/cities', {headers: this.authProvider.appendHeaders()})
    .subscribe( data => {
      this.cities = JSON.parse(JSON.stringify(data));
    })
  }


  onSelectChangeState(selectedValue: any){
    this.selectedState = selectedValue
    this.stateSelected = true;
    this.getCities()
  }

  onSelectChangeCity(selectedValue: any){
    this.selectedCity = selectedValue
  }

  addCondo() {
    this.imageUrl = this.imageProvider.imageUrl;
    this.data.imageUrl = this.imageUrl;
    this.data.state = this.selectedState.abbreviation;
    this.data.city = this.selectedCity.name;

    this.condoProvider.addCondo(this.data)
    .then(() => {
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }

}
