import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProgramDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-program-details',
  templateUrl: 'program-details.html',
})
export class ProgramDetailsPage {

  private program: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.program = navParams.get('program')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgramDetailsPage');
  }

}
