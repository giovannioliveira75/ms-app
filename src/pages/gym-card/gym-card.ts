import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GymCardProvider } from '../../providers/gym-card/gym-card';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the GymCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gym-card',
  templateUrl: 'gym-card.html',
})
export class GymCardPage {

  private role:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private authProvider:AuthProvider,
              private gymCardProvider: GymCardProvider,
              private alertsProvider: AlertsProvider
            ) {
  }

  ionViewCanEnter(){
    this.role = this.authProvider.userRole
  }

  removeGymCard(gymcard){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {

      if(!data){
        this.gymCardProvider.deleteGymcard(gymcard.id)
          .then(() => {
            this.alertsProvider.successAlert();
            this.navCtrl.setRoot(GymCardPage)
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

}
