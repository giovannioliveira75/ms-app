import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reports-detail',
  templateUrl: 'reports-detail.html',
})
export class ReportsDetailPage {

  private attendances:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.attendances = navParams.get('attendances')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportsDetailPage');
    console.log('Attendances', this.attendances)
  }

}
