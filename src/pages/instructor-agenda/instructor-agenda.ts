import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import * as moment from 'moment';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { AttendanceProvider } from '../../providers/attendance/attendance';
import { InstructorUserAttendancePage } from '../instructor-user-attendance/instructor-user-attendance';

/**
 * Generated class for the InstructorAgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-instructor-agenda',
  templateUrl: 'instructor-agenda.html',
})
export class InstructorAgendaPage {

  private role;
  private appointments:any;
  eventSource = [];
  viewTitle: string;
  selectedDay = new Date();
  private calendar = {
    mode: 'month',
    currentDate: new Date(),
    locale: 'pt-BR',
    dateFormatter: {
      formatMonthViewDay: function(date:Date) {
          return date.getDate().toString();
      },
      formatMonthViewDayHeader: function(date:Date) {
          return 'MonMH';
      },
      formatMonthViewTitle: function(date:Date) {
          return 'testMT';
      },
      formatWeekViewDayHeader: function(date:Date) {
          return 'MonWH';
      },
      formatWeekViewTitle: function(date:Date) {
          return 'testWT';
      },
      formatWeekViewHourColumn: function(date:Date) {
          return 'testWH';
      },
      formatDayViewHourColumn: function(date:Date) {
          return 'testDH';
      },
      formatDayViewTitle: function(date:Date) {
          return 'testDT';
      }
  }
  };

  constructor(public navCtrl: NavController,
              public app: App,
              public navParams: NavParams,
              private authProvider: AuthProvider,
              private alertCtrl: AlertController,
              private appointmentProvider: AppointmentProvider,
              private alertsProvider: AlertsProvider,
              private attendanceProvider: AttendanceProvider
            ) {
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter InstructorAgendaPage');
    this.role = this.authProvider.userRole;
    console.log("Role: ", this.role)
    this.role == 'instrutor' ?  this.getInstructorAppointments() : this.getAppointments();
  }

  changeMode(mode) {
    this.calendar.mode = mode;
  }

  loadEvents(){
    this.eventSource = this.populateAgenda()
    // this.markDisabled(new Date())
  }

  onEventSelected(event) {
    moment.locale('pt-BR');
    let title = event.title.split('|');
    let start = moment(event.startTime).format('LLL');
    let end = moment(event.endTime).format('LLL');

    let alert = this.alertCtrl.create({
      title: '' + title[0] + '<br>' + title[1],
      subTitle: 'De: ' + start + '<br>Até: ' + end,
      // message: 'Informações<br> Horário: <br>Ínicio: ' + startHour + '<br> Fim: ' + endHour ,
      buttons: [
        {
          text: 'PRESENÇA',
          handler: () => {

            if(moment(event.startTime).isSame(moment())) {
              let data = {
                'attend_date': moment(),
                'attend_time': moment(),
                'appointment': event.id,
              }
              this.attendanceProvider.addAttendance(data)
              .then(() => {
                this.alertsProvider.successAlert()
              })
              .catch( () => {
                this.alertsProvider.errorAlert()
              })
            } else {
              let alertErro = this.alertCtrl.create({
                title: 'Ooops!',
                message: 'Só é possível marcar presença no dia da atividade agendada.',
                buttons: [
                  {
                    text: 'FECHAR',
                    handler: () => {

                    }
                  }
                ]
              });
              alertErro.present()
            }
          }
        },
        {
          text: 'ALUNOS',
          handler: () => {
            // let eventTitle = title[0].trim();
            let eventId = event.id;
            this.app.getRootNav().push(InstructorUserAttendancePage, {appointment: eventId});
          }
        },
        {
          text: 'FECHAR',
          handler: () => {

          }
        },

      ]
    });
    alert.present();
  }

  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0,0,0,0)
    return date < current;
  }

  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  populateAgenda(){
    console.log("Populate Agenda")
    let events = [];
    for (let item of this.appointments){
      let date = item.start_date;
      let endDate = item.end_date;
      let time = item.start_time;
      let end_time = item.end_time;
      let startTime = moment(date).format('L') +' '+time;
      let endTime = moment(endDate).format('L') +' '+ end_time;
      let evtTitle = JSON.parse(JSON.stringify(item.activity));
      let evtInstructor = JSON.parse(JSON.stringify(item.name));

      events.push({
        id: item.id,
        title: ' ' + evtTitle + ' | Instrutor: ' + evtInstructor,
        startTime: new Date(startTime),
        endTime: new Date(endTime),
        allDay: false
      });

    }
    return events
  }

  getInstructorAppointments(){
    console.log("Instructor Appointments")
    this.appointmentProvider.getInstructorAppointments()
    .then(res => {
      this.appointments = JSON.parse(JSON.stringify(res))
    })
    .catch(err => this.alertsProvider.errorAlert())
  }

  getAppointments(){
    console.log("All Appointments")
    this.appointmentProvider.getAppointments()
    .then(res => {
      this.appointments = JSON.parse(JSON.stringify(res))
    })
    .catch(err => this.alertsProvider.errorAlert())
  }


}
