import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EventProvider } from '../../providers/event/event';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { UserProvider } from '../../providers/user/user';
import { CondoProvider } from '../../providers/condo/condo';


@Component({
  selector: 'page-event-update',
  templateUrl: 'event-update.html',
})
export class EventUpdatePage {

  private updated: boolean;
  private event: any;
  private imageUrl:any;
  private condos: any;
  private instructors:any;
  private hasCondos:boolean;
  private isActive:boolean;
  private today:string ;

  private data = {
    "imageUrl" : "",
    "title" : "",
    "description": "",
    "informations" : "",
    "event_text": "",
    "start_date" : "",
    "end_date" : "",
    "max_users": "",
    "min_users": "",
    "condos": { },
    "instructors": { },
    "active": {},
    "private": {}

  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private eventProvider: EventProvider,
              private imageProvider: ImageProvider,
              private alertsProvider: AlertsProvider,
              private userProvider: UserProvider,
              private condoProvider: CondoProvider
            ) {
              this.event = navParams.get('event');
  }

  ionViewWillEnter(){
    this.userProvider.getInstructors()
    .then( instructors => this.instructors = instructors)
    .catch(err => this.alertsProvider.errorAlert())

    this.condoProvider.getCondos()
    .then(condos => this.condos = condos)
    .catch(err => this.alertsProvider.errorAlert())

    this.today = new Date().toISOString()
    this.hasCondos = false
    this.isActive = false
    this.updated = false;
  }

  ionViewWillLeave(){
    if (this.updated && this.event.image.url)
      this.imageProvider.removeOld(this.event.image.url)
    // else if(this.imageProvider.imageUrl && !this.updated)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
    this.imageProvider.imageUrl = null
  }

  onSelectChangeCondos(selectedValue: any) {
    this.data.condos = selectedValue;
  }

  onSelectChangeUsers(selectedValue: any) {
    this.data.instructors = selectedValue;
  }

  updateEvent(){
    this.data.title == "" ? this.data.title = this.event.title : this.data.title;
    this.data.description == "" ? this.data.description = this.event.description : this.data.description;
    this.data.informations == "" ? this.data.informations = this.event.informations : this.data.informations;
    this.data.event_text == "" ? this.data.event_text=this.event.event_text : this.data.event_text;
    this.data.start_date == "" ? this.data.start_date= this.event.start_date : this.data.start_date;
    this.data.end_date == "" ? this.data.end_date = this.event.end_date : this.data.end_date;
    this.hasCondos ? this.data.private = true : this.data.private = false;
    this.isActive ? this.data.active = true : this.data.active = false;

    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.event.image.url

    this.eventProvider.updateEvent(this.data, this.event.id).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()

    }, () => {
      this.alertsProvider.errorAlert();
    });
  }

}
