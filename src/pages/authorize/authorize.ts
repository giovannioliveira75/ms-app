import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';
import { CondoProvider } from '../../providers/condo/condo';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-authorize',
  templateUrl: 'authorize.html',
})
export class AuthorizePage {

  private unauthorized: any;
  private data: {}
  private selectedCondo: boolean ;
  private condo:any;
  private condos:any;
  private role:string;

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthProvider,
    private userProvider: UserProvider,
    private condoProvider: CondoProvider,
    private alertCtrl: AlertController,
    public http: HttpClient
    )
    { this.role = this.authProvider.userRole; }

  ionViewCanEnter(){
    this.authProvider.userRole == "sindico" ? this.getUnauthorized() : this.getCondos();
  }

  ionViewWillEnter(){
    this.data = {};
    this.selectedCondo = false
  }

  getUnauthorized(){

      this.userProvider.getUserUnauthorized().then( (result) => {
        this.unauthorized = JSON.parse(JSON.stringify(result))
      });

  }

  getCondos() {
    this.condoProvider.getCondos().then(res => {
      this.condos = res;
    }).catch(err => {

    })
  }


  onSelectChange(selectedValue: any) {

    this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/condos/' + selectedValue.name + '/unauthorized', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          this.unauthorized = JSON.parse(JSON.stringify(data));
          this.selectedCondo = true;
          this.condo = selectedValue.name;
        }, (err) => {

      });
  }



  showConfirm(unauth) {
    let confirm = this.alertCtrl.create({
      title: 'Condominio',
      message: 'Após essa confirmação o usuário será liberado.\nDeseja liberar esse usuário?.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {

                this.role == 'sindico' ? this.condo = this.authProvider.userCondo : this.condo = this.condo;
                this.userProvider.updateAuthorizeUser(unauth, this.condo).then((result) => {
                  this.showAuthOk();
                }).catch( )

          }
        }
      ]
    });
    confirm.present();
  }

  showAuthOk(){
    let confirm = this.alertCtrl.create({
      title: 'Tudo certo!',
      message: 'O usuário foi liberado!',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.ionViewWillEnter()
          }
        }
      ]
    });
    confirm.present();
  }

}
