import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { ActivityPage } from '../activity/activity';
import { EventPage } from '../event/event';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ActivityPage;
  tab3Root = EventPage;
  tab4Root = ProfilePage;



  constructor() {

  }
}
