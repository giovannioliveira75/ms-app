import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FamilyProvider } from '../../providers/family/family';

/**
 * Generated class for the FamiliesUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-families-update',
  templateUrl: 'families-update.html',
})
export class FamiliesUpdatePage {

  private family: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public familyProvider: FamilyProvider) {
    this.family = navParams.get('family')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FamiliesUpdatePage');
  }

}
