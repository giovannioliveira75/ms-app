import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AttendanceProvider } from '../../providers/attendance/attendance';
import { AlertsProvider } from '../../providers/alerts/alerts';
import * as moment from 'moment';



@Component({
  selector: 'page-instructor-user-attendance',
  templateUrl: 'instructor-user-attendance.html',
})
export class InstructorUserAttendancePage {

  private users:any;
  private appointment:string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private attendanceProvider: AttendanceProvider,
              private alertsProvider: AlertsProvider
            ) {
    this.appointment = navParams.get('appointment')
  }

  ionViewCanEnter() {
    this.getUsersSubscribed()
  }

  ionViewWillEnter(){

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstructorUserAttendancePage');
  }

  getUsersSubscribed(){
    this.attendanceProvider.getUsersActivitiesSubscribeds(this.appointment)
    .then(res => this.users = JSON.parse(JSON.stringify(res)))
    .catch(() => this.alertsProvider.errorAlert())
    console.log("Users: ", this.users)
  }

  showConfirm(user, i){
    let data = {
      "user" : user,
      "appointment" : this.appointment,
      "attend_date": moment(),
      "attend_time": moment()
    };

    this.attendanceProvider.addUserAttendance(data)
    .then( () => this.alertsProvider.successAlert())
    .catch( () => this.alertsProvider.errorAlert())
    console.log("User index: ", i)
    this.users.splice(i, 1)

    };




}
