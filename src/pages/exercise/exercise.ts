import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ExerciseProvider } from '../../providers/exercise/exercise';
import { AuthProvider } from '../../providers/auth/auth';
import { ExerciseDetailPage } from '../exercise-detail/exercise-detail';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { ExerciseUpdatePage } from '../exercise-update/exercise-update';

/**
 * Generated class for the ExercisePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exercise',
  templateUrl: 'exercise.html',
})
export class ExercisePage {

  private role;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private exerciseProvider: ExerciseProvider,
              private authProvider: AuthProvider,
              private alertsProvider: AlertsProvider
            ) {
  }

  ionViewWillEnter(){
    this.role = this.authProvider.userRole;
  }

  editExercise(exercise){
    this.navCtrl.push(ExerciseUpdatePage, { exercise: exercise})
  }

  showDetails(exercise) {
    this.navCtrl.push(ExerciseDetailPage)
  }

  removeExercise(exercise){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.exerciseProvider.deleteExercise(exercise.id)
          .then(() => {
            this.alertsProvider.successAlert();
            this.navCtrl.setRoot(ExercisePage)
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }

  selectExerciseToAdd(){

  }
}
