import { Component } from '@angular/core';
import { IonicPage, ViewController, App, NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GymCardPage } from '../gym-card/gym-card';
import { CondoPage } from '../condo/condo';
import { ProgramPage } from '../program/program';
import { PromotionPage } from '../promotion/promotion';
import { PartnerPage } from '../partner/partner';
import { LoginPage } from '../login/login';
import { ExercisePage } from '../exercise/exercise';
import { GroupPage } from '../group/group';
import { ActivityPage } from '../activity/activity';
import { ChangeRolePage } from '../change-role/change-role';
import { AuthorizePage } from '../authorize/authorize';
import { TabsPage } from '../tabs/tabs';
import { InstructorAgendaPage } from '../instructor-agenda/instructor-agenda';
import { InstructorAgendaAddPage } from '../instructor-agenda-add/instructor-agenda-add';
import { ReportsPage } from '../reports/reports';
import { NotificationPage } from '../notification/notification';


@IonicPage()
@Component({
  templateUrl: 'popover.html'
})
export class PopoverPage {


  private role:string;
  private isEnabled:boolean;

  constructor(public viewCtrl: ViewController,
              public app: App,
              public authProvider: AuthProvider,
              public navCtrl: NavController,

            ) {

  }

  ionViewCanEnter(){
    this.role = this.authProvider.userRole;
    this.isEnabled = false;
  }

  start(){
    this.app.getRootNav().push(TabsPage)
    this.viewCtrl.dismiss()
  }

  activities(){
    this.app.getRootNav().push(ActivityPage);
    this.viewCtrl.dismiss()

  }

  authorize(){
    this.app.getRootNav().push(AuthorizePage);
    this.viewCtrl.dismiss()
  }

  changeRole(){
    this.app.getRootNav().push(ChangeRolePage);
    this.viewCtrl.dismiss();

  }

  condos(){
    this.app.getRootNav().push(CondoPage);
    this.viewCtrl.dismiss()
  }

  exercises(){
    this.app.getRootNav().push(ExercisePage);
    this.viewCtrl.dismiss()
  }

  gymCard(){
    this.app.getRootNav().push(GymCardPage);
    this.viewCtrl.dismiss()
  }

  groups(){
    this.app.getRootNav().push(GroupPage);
    this.viewCtrl.dismiss()
  }

  addInstructorAgenda(){
    this.app.getRootNav().push(InstructorAgendaAddPage)
    this.viewCtrl.dismiss()
  }

  instructorAgenda(){
    this.app.getRootNav().push(InstructorAgendaPage);
    this.viewCtrl.dismiss()
  }

  partners(){
    this.app.getRootNav().push(PartnerPage);
    this.viewCtrl.dismiss()

  }

  promotions(){
    this.app.getRootNav().push(PromotionPage);
    this.viewCtrl.dismiss()
  }

  switchCondo(){
    this.app.getRootNav().push(CondoPage);
    this.viewCtrl.dismiss()
  }

  programs(){
    this.app.getRootNav().push(ProgramPage);
    this.viewCtrl.dismiss()
  }

  reports() {
    this.app.getRootNav().push(ReportsPage)
    this.viewCtrl.dismiss()
  }

  sendNotification() {
    this.app.getRootNav().push(NotificationPage)
    this.viewCtrl.dismiss()
  }  





  logout() {
    this.authProvider.logout();
    this.viewCtrl.dismiss();
    this.navCtrl.setRoot(LoginPage);
  }
}
