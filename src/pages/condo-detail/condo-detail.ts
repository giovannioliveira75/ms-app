import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CondoProvider } from '../../providers/condo/condo';
import { CondoUpdatePage } from '../condo-update/condo-update';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the CondoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-condo-detail',
  templateUrl: 'condo-detail.html',
})
export class CondoDetailPage {

  private condo: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private condoProvider: CondoProvider, private alertsProvider:AlertsProvider) {
    this.condo = navParams.get('condo');
  }

  editCondo(condo){
    this.navCtrl.push(CondoUpdatePage, {condo: condo})
  }

  removeCondo(condo){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.condoProvider.deleteCondo(condo.id)
          .then(() => {
            this.alertsProvider.successAlert();
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }
}
