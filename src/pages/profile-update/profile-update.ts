import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CondoProvider } from '../../providers/condo/condo';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';



@Component({
  selector: 'page-profile-update',
  templateUrl: 'profile-update.html',
})
export class ProfileUpdatePage {


  private data = {
    "social_number" : "",
    "birth_date": "",
    "height":"",
    "weight": "",
    "building": "",
    "apartment": "",
    "condo": {
      "name": ""
    }

  };

  private condos: any;
  private hasAgreed: boolean;
  private profileUpdated: boolean;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private condoProvider: CondoProvider,
              private userProvider: UserProvider,
              private storage: Storage,
              private authProvider: AuthProvider
             ) {
  }

  ionViewWillEnter(){
    this.hasAgreed = false
  }


  ionViewDidLoad() {
    this.condoProvider.getCondos().then(res => this.condos = res).catch(err => console.log(err))
    this.storage.get('profileUpdated').then((value) => this.profileUpdated = value)
  }

  onSelectChange(selectedValue: any) {
    this.data.condo.name = selectedValue;
  }

  updateProfile(){

    this.userProvider.updateUserProfile(this.data).then( res => {
      this.storage.set('userCondo', this.data.condo.name)
      this.storage.set('access_token', '')
      this.navCtrl.setRoot(LoginPage);
    }).catch(err => {
      console.log(err)
    })
  }

  backToLogin(){
    this.authProvider.logout();
    this.navCtrl.setRoot(LoginPage)
  }
}
