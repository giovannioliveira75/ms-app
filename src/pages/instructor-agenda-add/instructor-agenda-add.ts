import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { CondoProvider } from '../../providers/condo/condo';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { EventProvider } from '../../providers/event/event';
import { AppointmentProvider } from '../../providers/appointment/appointment';



@Component({
  selector: 'page-instructor-agenda-add',
  templateUrl: 'instructor-agenda-add.html',
})
export class InstructorAgendaAddPage {

  private instructors:any;
  private condos: any;
  private hasCondo: boolean;
  private selectedTypes:string[];
  private types:any;
  private daysOfWeek:string[];
  private start_date:any;
  private end_date:any;
  private start_time:string;
  private end_time:string;
  private today:string;
  private data = {
    "instructor": "",
    "condo" : "",
    "type": "",
    "days" : "",
    "event" : {},
    "condoEvent": {},
    "activity": {},
    "program": {},
    "start_date": "",
    "end_date": "",
    "start_time":"",
    "end_time" : ""
  };


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertsProvider: AlertsProvider,
              private userProvider: UserProvider,
              private condoProvider: CondoProvider,
              private eventProvider: EventProvider,
              private appointmentProvider: AppointmentProvider
            )
  {

  }

  ionViewCanEnter(){
    this.userProvider.getInstructors()
    .then( res => this.instructors = JSON.parse(JSON.stringify(res) ))
    .catch( err => this.alertsProvider.errorAlert())

    this.condoProvider.getCondos()
    .then( res => this.condos = JSON.parse(JSON.stringify(res) ))
    .catch( err => this.alertsProvider.errorAlert())
  }

  ionViewWillEnter(){
    this.start_time = '05:00'
    this.end_time = '06:00'
    this.today = new Date().toISOString()
    this.daysOfWeek = ['Domingo', 'Segunda-feira', 'Terça-feira' , 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado' ]
    this.selectedTypes = [ 'Atividades', 'Eventos', 'Programas' ];
    this.hasCondo = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstructorAgendaAddPage');
  }

  onSelectChangeInstructors(SelectedValue:any){
    this.data.instructor = SelectedValue
  }

  onSelectChangeCondos(SelectedValue:any){
    this.data.condo =  SelectedValue
  }

  onSelectChangeSelectedTypes(SelectedValue:any){
    if (SelectedValue == 'Atividades') {
      this.getSelectedCondoActivities(this.data.condo)
      this.data.activity = true;
    } else if (SelectedValue == 'Eventos') {
      this.getSelectedCondoEvents(this.data.condo)
      this.data.condoEvent = true;
    } else {
      this.getSelectedCondoPrograms(this.data.condo)
      this.data.program = true;
    }

  }

  onSelectChangeTypes(SelectedValue:any){
    this.data.type = SelectedValue;
  }

  onSelectChangeDays(SelectedValue:any){
    this.data.days = SelectedValue

  }

  getSelectedCondoActivities(condo){
    this.condoProvider.getSelectedCondoActivities(condo)
    .then( res => this.types = JSON.parse(JSON.stringify(res) ))
    .catch( err => this.alertsProvider.errorAlert())

  }

  getSelectedCondoEvents(condo){
    this.condoProvider.getSelectedCondoEvents(condo)
    .then( res => this.types = JSON.parse(JSON.stringify(res) ))
    .catch( err => this.alertsProvider.errorAlert())

  }

  getEvents(){
    this.eventProvider.getEvents()
    .then( res => this.types = JSON.parse(JSON.stringify(res) ))
    .catch( err => this.alertsProvider.errorAlert())

  }

  getSelectedCondoPrograms(condo){
    this.condoProvider.getSelectedCondoPrograms(condo)
    .then( res => this.types = JSON.parse(JSON.stringify(res) ))
    .catch( err => this.alertsProvider.errorAlert())

  }

  addInstructorAgenda(){
    this.data.start_time = this.start_time;
    this.data.end_time = this.end_time;
    this.data.start_date = this.start_date;
    this.data.end_date = this.end_date;

    this.appointmentProvider.addAppointment(this.data)
    .then(() => {
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }

}
