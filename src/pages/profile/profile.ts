import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { UserProfileUpdatePage } from '../user-profile-update/user-profile-update';
import { AlertsProvider } from '../../providers/alerts/alerts';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  private userData:any ;


  constructor(public navCtrl: NavController, public navParams: NavParams,private userProvider: UserProvider, private alertsProvider:AlertsProvider) {

  }


  ionViewCanEnter() {    
    this.getUserProfile()
  }

  ionViewWillEnter(){
    console.log("Userdata will enter: ", this.userData)
  }

  editUser(user){
    this.navCtrl.push(UserProfileUpdatePage, {user: user})
  }

  getUserProfile(){
    this.userProvider.getUserProfile()
    .then((res) => {
      this.userData = JSON.parse(JSON.stringify(res)) 
      console.log("UserData get Profile: ", this.userData)     
      })
    .catch(() => this.alertsProvider.errorAlert())  
  }

}
