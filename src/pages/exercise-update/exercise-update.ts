import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ExerciseProvider } from '../../providers/exercise/exercise';

/**
 * Generated class for the ExerciseUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exercise-update',
  templateUrl: 'exercise-update.html',
})
export class ExerciseUpdatePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public exerciseProvider: ExerciseProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExerciseUpdatePage');
  }

  updateExercise(){

  }

}
