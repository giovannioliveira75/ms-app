import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PromotionUpdatePage } from '../promotion-update/promotion-update';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the PromotionDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotion-details',
  templateUrl: 'promotion-details.html',
})
export class PromotionDetailsPage {

  private promotion:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private promotionProvider: PromotionProvider,
              private imageProvider: ImageProvider,
              private alertsProvider: AlertsProvider
            ) {
    this.promotion = navParams.get('promotion')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionDetailsPage');
  }

  editPromotion(promotion){
    this.navCtrl.push(PromotionUpdatePage, {promotion: promotion} );
  }

  removePromotion(promotion){
    this.promotionProvider.deletePromotion(promotion).then(( result ) => {
      this.imageProvider.removeOld(promotion.image.url)
      this.alertsProvider.successAlert()
    }, (err) => {
      this.alertsProvider.errorAlert()
    }
  );
  }

}
