import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CondoProvider } from '../../providers/condo/condo';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { ImageProvider } from '../../providers/image/image';


/**
 * Generated class for the CondoUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-condo-update',
  templateUrl: 'condo-update.html',
})
export class CondoUpdatePage {

  private data = {
    "name": "",
    "condo_info" : "",
    "imageUrl": ""
  }
  private stateSelected:boolean = false;
  private selectedState: any;
  private selectedCity: any;
  private imageUrl:any;
  private updated: boolean;
  private condo:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private condoProvider: CondoProvider,
              private alertsProvider:AlertsProvider,
              private imageProvider:ImageProvider,

            ) {
    this.condo = navParams.get('condo')
  }

  ionViewWillEnter(){    
    this.updated = false;
  }

  ionViewWillLeave(){
    if (this.updated && this.condo.image.url)
      this.imageProvider.removeOld(this.condo.image.url)
    // else if(this.imageProvider.imageUrl && !this.updated)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
  }
  

  updateCondo(){
    this.data.name == "" ? this.data.name = this.condo.name : this.data.name;
    this.data.condo_info == "" ? this.data.condo_info=this.condo.condo_info : this.data.condo_info;
    
    if (this.imageProvider.imageUrl){
      this.imageUrl = this.imageProvider.imageUrl;
      this.data.imageUrl = this.imageUrl;
    } else this.data.imageUrl = this.condo.image.url

    this.condoProvider.updateCondo(this.data, this.condo.id).then(() => {
      this.updated = true;
      this.alertsProvider.successAlert();
      this.navCtrl.popToRoot()
    }, () => {
      this.alertsProvider.errorAlert();
    });
  }

}
