import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActivityProvider } from '../../providers/activity/activity';
import { CondoProvider } from '../../providers/condo/condo';
import { ImageProvider } from '../../providers/image/image';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the ActivityAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-activity-add',
  templateUrl: 'activity-add.html',
})
export class ActivityAddPage {

  private data = {
    "imageUrl" : "",
    "title" : "",
    "description": "",
    "informations" : "",
    "activity_text": "",
    "start_date" : "",
    "end_date" : "",
    "condos": { }
  };

  private today:string;
  private condos: any;
  private imageUrl:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private activityProvider: ActivityProvider,
              private condoProvider: CondoProvider,
              private imageProvider: ImageProvider,
              private alertsProvider: AlertsProvider) {

  }



  ionViewCanEnter(){
    return new Promise((resolve, reject) => {
      this.condoProvider.getCondos().then( (result) => {
        this.condos = result;
        resolve(result);
      }, (err) => {
        reject(err);

      });
    });
  }

  ionViewWillEnter(){
    this.today = new Date().toISOString();
  }


  onSelectChange(selectedValue: any) {
    this.data.condos = selectedValue;
  }

  addActivity(){

    this.imageUrl = this.imageProvider.imageUrl;
    this.data.imageUrl = this.imageUrl;

    this.activityProvider.addActivity(this.data).then( () => {
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }


}
