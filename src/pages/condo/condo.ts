import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CondoProvider } from '../../providers/condo/condo';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../../providers/auth/auth';
import { CondoDetailPage } from '../condo-detail/condo-detail';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { CondoUpdatePage } from '../condo-update/condo-update';
import { CondoAddPage } from '../condo-add/condo-add';

/**
 * Generated class for the CondoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-condo',
  templateUrl: 'condo.html',
})
export class CondoPage {

  private userCondos: {};
  private condos = {};
  private showUserCondos:boolean = false;
  private role;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              private condoProvider: CondoProvider,
              private userProvider: UserProvider,
              private storage: Storage,
              private authProvider: AuthProvider,
              private alertsProvider: AlertsProvider
            ) {
  }

  create() {
    this.navCtrl.push(CondoAddPage);
  }

  ionViewCanEnter() {
    return new Promise((resolve, reject) => {
      this.condoProvider.getCondos().then( (result) => {
        this.condos = result;
        resolve(result);
        console.log(this.condos);
      }, (err) => {
        reject(err);
        console.log(err);
      });
    });
  }

  ionViewWillEnter(){
    this.role = this.authProvider.userRole;
  }

  ionViewDidLoad() {
    this.checkUserCondos();
  }

  checkUserCondos(){
    this.storage.get('userCondo').then( value=> {
      if (value == [] || value == "" || value == null) {
        this.getUserCondos();
      }
    });
  }

  addUserToCondo(condo){
    this.showConfirm(condo);
  }

  getUserCondos(){
    this.userProvider.getUserCondos().then(result => {
      if (Object.keys(result).length > 0 ){
        this.userCondos = result;
        this.showUserCondos = true;
      } else this.showUserCondos = false;
    });
  }

  showConfirm(condo) {
    let confirm = this.alertCtrl.create({
      title: 'Condominio',
      message: 'Esse condominio será usado como principal.\nAs informações do aplicativo serão baseadas nele.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.storage.set('userCondo', condo.name)
            this.authProvider.userCondo = condo.name
            this.navCtrl.popToRoot()
          }
        }
      ]
    });
    confirm.present();
  }

  switchMain(condo){
    this.showConfirm(condo);
  }

  showDetails(condo){
    this.navCtrl.push(CondoDetailPage, {condo: condo})
  }

  editCondo(condo){
    this.navCtrl.push(CondoUpdatePage, {condo: condo})
  }

  removeCondo(condo){
    let data = this.alertsProvider.confirmAlert()
    data.present()
    data.onDidDismiss((data) => {
      if(!data){
        this.condoProvider.deleteCondo(condo.id)
          .then(() => {
            this.alertsProvider.successAlert();
          })
          .catch(() => this.alertsProvider.errorAlert())
      }
    })
  }
}
