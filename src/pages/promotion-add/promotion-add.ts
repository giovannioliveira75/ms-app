import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImageProvider } from '../../providers/image/image';
import { PartnerProvider } from '../../providers/partner/partner';
import { PromotionProvider } from '../../providers/promotion/promotion';
import { AlertsProvider } from '../../providers/alerts/alerts';

/**
 * Generated class for the PromotionAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promotion-add',
  templateUrl: 'promotion-add.html',
})
export class PromotionAddPage {

  private data = {
    "imageUrl": "",
    "title": "",
    "description" : "",
    "promotion_text": "",
    "start_date": "",
    "end_date": "",
    "active": {},
    "partner": ""
  }

  private today:string;
  private isActive:boolean;
  private imageUrl:any;
  private partners:any;
  private created:boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private imageProvider: ImageProvider,
              private partnerProvider: PartnerProvider,
              private promotionProvider: PromotionProvider,
              private alertsProvider: AlertsProvider
            ) {
  }

  ionViewCanEnter(){
    this.partnerProvider.getPartners()
    .then(res => this.partners = JSON.parse(JSON.stringify(res)))
    .catch(() => this.alertsProvider.errorAlert())
  }

  ionViewWillEnter(){
    this.today = new Date().toISOString()
    this.isActive = false
    this.created = false
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionAddPage');
  }

  ionViewWillLeave(){
    if (!this.created && this.imageProvider.imageUrl)
      this.imageProvider.removeOld(this.imageProvider.imageUrl)
    this.imageProvider.imageUrl = null
    
  }

  onSelectPartnerChange(SelectedValue: any){
    this.data.partner = SelectedValue
  }

  addPromotion(){

    this.imageUrl = this.imageProvider.imageUrl;
    this.data.imageUrl = this.imageUrl;
    this.data.active = this.isActive;

    this.promotionProvider.addPromotion(this.data).then( () => {
      this.created = true
      this.alertsProvider.successAlert()
      this.navCtrl.popToRoot()
    })
    .catch(() => this.alertsProvider.errorAlert())
  }


}
