import { NgModule } from '@angular/core';
import { TopLeftToolbarComponent } from './top-left-toolbar/top-left-toolbar';
import { IonicModule } from 'ionic-angular';
import { UploadFormComponent } from './upload-form/upload-form';

@NgModule({
	declarations: [TopLeftToolbarComponent,
    UploadFormComponent],
	imports: [IonicModule.forRoot(TopLeftToolbarComponent,UploadFormComponent )],
	exports: [TopLeftToolbarComponent,
    UploadFormComponent]
})
export class ComponentsModule {}
