import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImageProvider } from '../../providers/image/image';
import { Upload } from '../../providers/image/upload';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';





/**
 * Generated class for the UploadFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'upload-form',
  templateUrl: 'upload-form.html'
})

export class UploadFormComponent {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private imageProvider: ImageProvider,
    private db: AngularFireDatabase,
    private afs: AngularFireStorage) {}

  public progress: Observable<number>;
  public downloadURL: Observable<string>;
  selectedFiles: FileList;
  currentUpload: Upload;

  fileName:any;

  detectFiles(event) {
    // if(this.imageProvider.imageUrl)
    //   this.imageProvider.removeOld(this.imageProvider.imageUrl)
    this.selectedFiles = event.target.files;
    this.uploadSingle();

  }

  uploadSingle() {
    let file = this.selectedFiles.item(0)
    this.currentUpload = new Upload(file);
    this.uploadFileToStorage(this.currentUpload)

  }

  uploadFileToStorage(upload: Upload) {

    this.fileName = `${this.generateUUID()}.jpg`;
    const ref = this.afs.ref(this.fileName);
    const task = this.afs.upload(this.fileName,upload.file);
    this.progress = task.percentageChanges();

    task.snapshotChanges().pipe(
      finalize(() => {
       ref.getDownloadURL().subscribe(url => { this.imageProvider.imageUrl = url})
      })
    ).subscribe()

  }

  saveInfoToDatabase(metainfo){
    let info = {
      created: metainfo.timeCreated,
      url: metainfo.downloadURLs[0],
      fullPath: metainfo.fullPath,
      contentType: metainfo.contentType
    }
    return this.db.list(this.fileName).push(info);
  }

  private generateUUID(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }




}
