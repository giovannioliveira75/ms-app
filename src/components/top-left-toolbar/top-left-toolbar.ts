import { Component } from '@angular/core';
import { PopoverController, NavController } from 'ionic-angular';
import { PopoverPage } from '../../pages/popover/popover';
import { ActivityAddPage } from '../../pages/activity-add/activity-add';
import { HealthTipAddPage } from '../../pages/health-tip-add/health-tip-add';
import { EventAddPage } from '../../pages/event-add/event-add';
import { AuthProvider } from '../../providers/auth/auth';
import { CondoAddPage } from '../../pages/condo-add/condo-add';
import { ExerciseAddPage } from '../../pages/exercise-add/exercise-add';
import { GroupAddPage } from '../../pages/group-add/group-add';
import { ProgramAddPage } from '../../pages/program-add/program-add';
import { InstructorAgendaAddPage } from '../../pages/instructor-agenda-add/instructor-agenda-add';
import { PartnerAddPage } from '../../pages/partner-add/partner-add';
import { PromotionAddPage } from '../../pages/promotion-add/promotion-add';

@Component({
  selector: 'top-left-toolbar',
  templateUrl: 'top-left-toolbar.html'
})
export class TopLeftToolbarComponent {

  private page: string;
  private role:string;

  constructor(public popoverCtrl:PopoverController, public navCtrl: NavController, private authProvider: AuthProvider) {
    this.role = this.authProvider.userRole;
  }

  presentPopover(openPop) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: openPop
    });
  }

  create() {
    this.page = this.navCtrl.getActive().name;

    switch (this.page) {
      case "ActivityPage":
        this.navCtrl.push(ActivityAddPage);
        break;
      case "InstructorAgendaPage":
        this.navCtrl.push(InstructorAgendaAddPage);
        break;
      case "HomePage":
        this.navCtrl.push(HealthTipAddPage);
        break;
      case "EventPage":
        this.navCtrl.push(EventAddPage);
        break;
      case "CondoPage":
        this.navCtrl.push(CondoAddPage);
        break;
      case "ExercisePage":
        this.navCtrl.push(ExerciseAddPage);
        break;
      case "GroupPage":
        this.navCtrl.push(GroupAddPage);
        break;
      case "PartnerPage":
        this.navCtrl.push(PartnerAddPage);
        break;
      case "ProgramPage":
        this.navCtrl.push(ProgramAddPage);
        break;
      case "PromotionPage":
        this.navCtrl.push(PromotionAddPage);
        break;
      default:
        break;
    }
  }

}
