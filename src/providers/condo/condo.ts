import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the CondoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CondoProvider {




  constructor(public http: HttpClient, public storage: Storage, public authProvider: AuthProvider) {
    console.log('CondoProvider');
  }

  // Condos
  getCondos() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'condos', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getCondoActivities() {
    return new Promise ( (resolve, reject) => {

      this.storage.get('userCondo').then((value) => {
        let currentCondo = value;
        this.http.get(this.authProvider.apiUrl+'condos/' + currentCondo + '/activities', { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
      });
    });
  }

  getSelectedCondoActivities(condo) {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'condos/' + condo + '/activities', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });

  }

  getSelectedCondoEvents(condo) {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'condos/' + condo + '/events', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });

  }

  getSelectedCondoPrograms(condo) {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'condos/' + condo + '/programs', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });

  }

  getCondoEvents() {
    return new Promise ( (resolve, reject) => {

      this.storage.get('userCondo').then((value) => {
        let currentCondo = value;
        this.http.get(this.authProvider.apiUrl+'condos/' + currentCondo + '/events', { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
      });
    });
  }

  getCondoPrograms() {
    return new Promise ( (resolve, reject) => {

      this.storage.get('userCondo').then((value) => {
        let currentCondo = value;
        this.http.get(this.authProvider.apiUrl+'condos/' + currentCondo + '/programs', { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
      });
    });
  }

  getCondoPromotions() {
    return new Promise ( (resolve, reject) => {

      this.storage.get('userCondo').then((value) => {
        let currentCondo = value;
        this.http.get(this.authProvider.apiUrl+'condos/' + currentCondo + '/promotions', { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
      });
    });
  }


  addCondo(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'condos', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateCondo(data, condo) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'condos/'+ condo ,data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteCondo(condo) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'condos/'+ condo, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteCondoActivity(activity) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'condos/'+ activity.pivot.condos_id + "/activities/" + activity.id, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteCondoEvent(event) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'condos/'+ event.pivot.condos_id + "/events/" + event.id, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteCondoPromotion(promotion) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'condos/'+ promotion.pivot.condos_id + "/events/" + promotion.id, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }



}
