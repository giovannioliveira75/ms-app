import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class AppointmentProvider {

  constructor(public http: HttpClient, private authProvider: AuthProvider) {

  }

  // APPOINTMENTS
  getAppointments() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'appointments', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getInstructorAppointments(){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'appointments/instructors/'+ this.authProvider.userEmail, { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  addAppointment(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'appointments', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateAppointment(data, appointment) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'appointments/'+ appointment ,data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteAppointment(appointment) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'appointments/'+ appointment, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
