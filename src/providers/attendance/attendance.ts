import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { AuthProvider } from '../auth/auth';


@Injectable()
export class AttendanceProvider {

  constructor(public http: HttpClient, private authProvider: AuthProvider) {
    console.log('Hello AttendanceProvider Provider');
  }

  getAttendances() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'attendances', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getAttendancesByInstructor(startDate, endDate, instructor) {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'attendances/instructors/'+ instructor + '/appointments', { headers: this.authProvider.appendHeaders(), params: {startDate, endDate, instructor} })
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getAttendancesByUser(startDate, endDate, user){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'attendances/users/'+ user + '/appointments', { headers: this.authProvider.appendHeaders(), params: {startDate, endDate, user} })
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getInstructorsAttendances(){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'attendances/instructors/'+ this.authProvider.userEmail + '/appointments', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getUsersActivitiesAttendances(){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'attendances/users/'+ this.authProvider.userEmail + '/appointments', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getUsersActivitiesSubscribeds(appointment){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'attendances/condos/'+ this.authProvider.userCondo +'/appointments/'+ appointment + '/subscribeds', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  addAttendance(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'attendances/appointments', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserAttendance(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'attendances/appointments/users/attendances', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }



  updateAttendance(data, attendance) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'attendances/'+ attendance ,data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteAttendance(attendance) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'attendances/'+ attendance, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }
}
