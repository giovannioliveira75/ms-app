import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the GymCardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GymCardProvider {


  constructor(public http: HttpClient, public storage: Storage, public authProvider: AuthProvider) {
    }

  // GymCards

  getGymcards() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'gymcards', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }


  addGymcard(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'gymcards', data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateGymcard(data, gymCard) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'gymcards/'+ gymCard ,data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteGymcard(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'gymcards/'+ data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
