import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the RoleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RoleProvider {

  constructor(public http: HttpClient, private authProvider: AuthProvider) {
    console.log('Hello RoleProvider Provider');
  }

  getRoles() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'roles', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        console.log("Users ", data)
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });

  }

  updateRoles(data){
    return new Promise ( (resolve, reject) => {
      this.http.put(this.authProvider.apiUrl+'roles/emails/'+ this.authProvider.userEmail + '/roles/changes/' , data ,{ headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
            resolve(data);
          }, (err) => {
          console.log(err)
        });
    });
  }
}
