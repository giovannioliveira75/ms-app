import { Injectable } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';

@Injectable()
export class ImageProvider {

  public imageUrl:any;

  constructor(private afs: AngularFireStorage) {}

  removeOld(url){
    let profile = "https://firebasestorage.googleapis.com/v0/b/teste-a71ed.appspot.com/o/profile2-512.png?alt=media&token=f04073ad-eb6a-44ab-b912-4617b7153cae"
    if (url !== profile) {
      this.afs.storage.refFromURL(url).delete()
      .then((res) => console.log("Deletado" ,res) )
      .catch((err) => console.log("Não Deletado" , err))
    }
    
  }

}


