import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the EventProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventProvider {


  constructor(public http: HttpClient, public storage: Storage, public authProvider: AuthProvider) {
     }

  // EVENTS

  getEvents() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'events', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getPublicEvents() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'events/emails/'+ this.authProvider.userEmail + '/events/publisheds', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  addEvent(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'events', data, { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateEvent(data, event) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'events/'+ event ,data, { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteEvent(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'events/'+ data, { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }


}
