import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the AddressProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AddressProvider {

  constructor(public http: HttpClient, public storage: Storage, public authProvider:AuthProvider) {
 }

  getAddresses() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'addresses', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  addAddress(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'addresses', data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateAddress(data, address){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'addresses/'+ address ,data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteAddress(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'condos/'+ data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
