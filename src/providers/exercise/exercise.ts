import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the ExerciseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ExerciseProvider {


  constructor(public http: HttpClient, public authProvider: AuthProvider) {
    }

   // Exercices

   getExercises() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'exercises', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  addExercise(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'exercises', data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateExercise(data, exercise) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'exercises/'+ exercise ,data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteExercise(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'exercise/'+ data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }
}
