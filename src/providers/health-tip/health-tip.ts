import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the HealthTipProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HealthTipProvider {

  constructor(public http: HttpClient, public storage: Storage, public authProvider: AuthProvider) {
    }



  // HEALTH TIPS

  getHealthTips() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'healthTips', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
         resolve(data);
       }, (err) => {
         reject(err);
       });
    });

  }

  addHealthTip(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'healthTips', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateHealthTip(data, healthTip) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'healthTips/'+ healthTip ,data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteHealthTip(healthTip) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'healthTips/'+ healthTip, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
