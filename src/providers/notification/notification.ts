import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationProvider {

  private notificationToken = "AIzaSyAn1FkEKYa0BPnc-Rxx263cpuIsXj0gaPQ";
  
  constructor(public http: HttpClient) {
    console.log('Hello NotificationProvider Provider');
  }


  //Add this function and call it where you want to send it.
  sendNotification( notificationTitle, notificationText ) {  
    return new Promise ( (resolve, reject) => {
      let body = {        
          "notification":{
            "title": notificationTitle,
            "body": notificationText,
            "sound":"default",
            "click_action":"FCM_PLUGIN_ACTIVITY",
            "icon":"../assets/imgs/icon.png"
          }, 
          "data": {
            "title": notificationTitle,
            "body": notificationText
          },        
          "to": "/topics/all"
        }

        let options = new HttpHeaders().set('Content-Type','application/json');
        
        this.http.post("https://fcm.googleapis.com/fcm/send", body, {
          headers: options.set('Authorization', 'key=' + this.notificationToken),
        }).subscribe(resp => {
          resolve(resp)
        }, (err) => {
          reject(err)
        });
      });
    }
}
