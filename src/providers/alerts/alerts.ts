import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';



@Injectable()
export class AlertsProvider {

  constructor(public alertCtrl: AlertController
              ) {
    console.log('Hello AlertsProvider Provider');
  }

  errorAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Oops! Algo deu errado.',
      message: 'Não foi possivel realizar a operação neste momento. Tente novamente mais tarde.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }


        }
      ]
    });
    confirm.present();

  }

  notAvailableAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Oops!',
      message: 'Por enquanto esse recurso está indisponível',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }


        }
      ]
    });
    confirm.present();

  }

  successAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'A operação foi realizada sem erros!',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }


        }
      ]
    });
    confirm.present();
  }

  confirmAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Pense bem!',
      message: 'Você tem certeza de que gostaria de excluir esse dado?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            confirm.dismiss(true)
            return false
          }
        },
        {
          text: 'Ok',
          handler: () => {
            confirm.dismiss(false)
            return false
          }
        }
      ]
    });
    return confirm
  }

  loginErrorAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Autenticação inválida.',
      message: 'Usuário ou senha incorretos. Por favor verifique os dados de login.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }


        }
      ]
    });
    confirm.present();
  }

  successSubscribeAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Ótimo!!',
      message: 'Agora você ja está participando.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }
        }
      ]
    });
    confirm.present();
  }

  successUnsubscribeAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Que pena!!',
      message: 'Agora você não participa mais, mas esperamos o seu retorno.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }
        }
      ]
    });
    confirm.present();
  }

  errorSubscribeAlert(){
    let confirm = this.alertCtrl.create({
      title: 'OOps!!',
      message: 'Você ja está participando.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }
        }
      ]
    });
    confirm.present();
  }

  errorUnsubscribeAlert(){
    let confirm = this.alertCtrl.create({
      title: 'OOps!!',
      message: 'Você já não participa, mas esperamos o seu retorno.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => { }
        }
      ]
    });
    confirm.present();
  }


}
