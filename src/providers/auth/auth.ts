import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';


interface LoginResponse {
  token: {
    access_token: string;
    refresh_token: string;
  }
  user: {
    email: string;
    name:string;
    authorized:boolean;
    profileUpdated:boolean;
    image: { url:string;};
    building: string;
    apartment: string;
    role: { name:string;};
    activities: {};
    condos: {};
    events: {};
    gymcards: {};
    programs: {};

  }

}

@Injectable()
export class AuthProvider {

  //apiUrl = "http://api.manofitness.com";
  //https://manofitnessapi.herokuapp.com
  public apiUrl = "https://manofitness.igniteweb.com.br/api/v1/";
  private oauthTokenUrl = "https://manofitness.igniteweb.com.br/api/v1/auth";
  private contentType = 'Content-Type';
  private content = 'application/json';

  public userToken : any;
  public userRefreshToken:any;
  public userEmail: string;
  public userName: string;
  public authorized:boolean;
  public profileUpdated: boolean;
  public userRole: any;
  public userActivities: any;
  public userCondos: any;
  public userEvents:any;
  public userGymcards: any;
  public userPrograms:any;
  public profileUrl:string;
  public building: string;
  public apartment: string;

  public userCondo: string;

  constructor(public http: HttpClient, public storage: Storage) {
    }

  checkAuth() {
    return new Promise((resolve, reject) => {
      this.storage.get('access_token').then((value) => {
        this.userToken = value;
        this.getStorageEmail();
        this.getStorageRole();
        this.getStorageCondo();
        this.getStorageCondos();

        this.http.get(this.apiUrl+'healthTips', { headers: this.appendHeaders()})
          .subscribe(res => {

            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
    });
  }

  checkProfileStatus(){
    this.http.get(this.apiUrl + 'users/emails/'+ this.userEmail + '/profileupdated', {headers: this.appendHeaders()})
      .subscribe(res => {
        this.storage.set('profileUpdated', res)
      }, (err) => {
        console.log(err)
      })
  }


  login(credentials) {

    return new Promise( (resolve, reject) => {
      let headers = new HttpHeaders();
      headers = headers.append(this.contentType, this.content);

      this.http.post<LoginResponse>(this.oauthTokenUrl, credentials, {headers: headers})
        .subscribe(res => {

          this.userToken = "Bearer " + res.token.access_token;
          this.userRefreshToken = res.token.refresh_token;
          this.userEmail = res.user.email;
          this.userName = res.user.name;
          this.authorized = res.user.authorized;
          this.profileUpdated = res.user.profileUpdated;
          this.profileUrl = res.user.image.url;
          this.building = res.user.building;
          this.apartment = res.user.apartment;
          this.userRole = res.user.role.name;
          this.userActivities = res.user.activities;
          this.userCondos = res.user.condos;
          this.userEvents = res.user.events;
          this.userGymcards = res.user.gymcards;
          this.userPrograms = res.user.programs;

          Object.keys(this.userCondos).length > 0 ? this.userCondo = this.userCondos[0].name: this.userCondo = "";


          this.storage.set('access_token',this.userToken)
          this.storage.set('refresh_token', this.userRefreshToken)
          this.storage.set('email', this.userEmail)
          this.storage.set('name', this.userName)
          this.storage.set('authorized', this.authorized)
          this.storage.set('profileUpdated', this.profileUpdated)
          this.storage.set('profileUrl', this.profileUrl)
          this.storage.set('building', this.building)
          this.storage.set('apartment', this.apartment)
          this.storage.set('role', this.userRole)
          this.storage.set('userActivities', this.userActivities)
          this.storage.set('userCondo', this.userCondo)
          this.storage.set('userCondos', this.userCondos)
          this.storage.set('userEvents', this.userEvents)
          this.storage.set('userGymcards', this.userGymcards)
          this.storage.set('userPrograms', this.userPrograms)



          resolve(res);

        }, (err) => {
          reject(err);
        });

    });
  }

  signUp(data) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers = headers.append(this.contentType, this.content);

      this.http.post(this.apiUrl+'users', data, { headers: headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  logout() {
    this.storage.set('access_token', '')
    this.storage.set('refresh_token', '')
    this.storage.set('email', '')
    this.storage.set('name', '')
    this.storage.set('authorized', '')
    this.storage.set('profileUpdated', '')
    this.storage.set('profileUrl', '')
    this.storage.set('building', '')
    this.storage.set('apartment', '')
    this.storage.set('role', '')
    this.storage.set('userActivities', '')
    this.storage.set('userCondos', '')
    this.storage.set('userEvents', '')
    this.storage.set('userGymcards', '')
    this.storage.set('userPrograms', '')
  }

  appendHeaders(){
    let headers = new HttpHeaders();
    return headers = headers.append('Authorization', this.userToken);
  }

  appendContentType(){
    let headers = new HttpHeaders();
    headers = headers.append(this.contentType, this.content);
  }

  getStorageEmail(){
    this.storage.get('email').then((value) => {
      this.userEmail = value;
    });
  }

  getStorageCondo() {
    this.storage.get('userCondo').then((value) => {
      this.userCondo = value;
    });
  }

  getStorageCondos() {
    this.storage.get('userCondos').then((value) => {
      this.userCondos = value;
    });
  }

  getStorageRole() {
    this.storage.get('role').then((value) => {
      this.userRole = value;
    });
  }


}
