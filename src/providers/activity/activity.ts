import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class ActivityProvider {


  constructor(public http: HttpClient, public authProvider: AuthProvider) {
  }


  // ATIVIDADES
  getActivities() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'activities', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  addActivity(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'activities', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateActivity(data, activity) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'activities/'+ activity ,data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteActivity(activity) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'activities/'+ activity, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
