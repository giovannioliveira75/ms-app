import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the FamilyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FamilyProvider {


  constructor(public http: HttpClient, public authProvider: AuthProvider) {
    }


 // Families

 getFamilies() {
  return new Promise ( (resolve, reject) => {

    this.http.get(this.authProvider.apiUrl+'families', { headers: this.authProvider.appendHeaders()})
    .subscribe( data => {
      resolve(data);
    }, (err) => {
      reject(err);
    });
  });
}

addFamily(data) {
  return new Promise ( (resolve, reject) => {
    this.authProvider.appendContentType();
    this.http.post(this.authProvider.apiUrl+'families', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
  });
}

updateFamily(data, family) {
  return new Promise ( (resolve, reject) => {
    this.authProvider.appendContentType();
    this.http.put(this.authProvider.apiUrl+'families/'+ family ,data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
  });
}

deleteFamily(data) {
  return new Promise ( (resolve, reject) => {
    this.authProvider.appendContentType();
    this.http.delete(this.authProvider.apiUrl+'families/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
  });
}
}
