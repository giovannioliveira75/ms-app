import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: HttpClient, public storage: Storage, public authProvider: AuthProvider) {
     }

  // USERS REST GET
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  getInstructors(){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/instructors', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

  getUsers() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });

  }

  getUserActivities() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/activities', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserAddresses() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/addresses', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserCondos() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/condos', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
        this.storage.set('condos', data);
        resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserEvents() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/events', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserFamilies() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/families', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserGymCards(){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/gymcards', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserHealthTips() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/healthTips', { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserProfile() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserPrograms() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/programs', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserPromotions(){
    return new Promise ( (resolve, reject) => {


      this.http.get(this.authProvider.apiUrl+'users/emails/' + this.authProvider.userEmail + '/promotions', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserRoles() {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/roles', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {

          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserSubscribedActivity(activity) {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/activities/subscribeds', { headers: this.authProvider.appendHeaders(), params: { activity }})
      .subscribe( data => {

          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserSubscribedEvent(event) {
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/events/subscribeds', { headers: this.authProvider.appendHeaders(), params: { event }})
      .subscribe( data => {

          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserSubscribedProgram(program) {
    return new Promise ( (resolve, reject) => {
      this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/programs/subcribeds', { headers: this.authProvider.appendHeaders(), params: { program }})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  getUserUnauthorized(){
    return new Promise ( (resolve, reject) => {

      this.http.get(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/condos/' + this.authProvider.userCondo + '/unauthorized', { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {

          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  // USERS REST POST
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  addUser(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.authProvider.apiUrl+'/users', data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserActivities(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/activities/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserAddresses(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/addresses/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserCondos(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/condos', data, {headers: this.authProvider.appendHeaders()})
      .subscribe(res => {
        this.storage.set('condo', data.name);
        resolve(res);
      }, (err) => {
        reject(err);
      });
    });
  }

  addUserEvents(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/events/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserFamilies(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/families/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserGymCards(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/gymcards/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserHealthTips(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();

      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/healthtips/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserPrograms(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/programs/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserPromotions(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/promotions/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  addUserRoles(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.post(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/roles/', data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  // USERS REST PUT
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  updateAuthorizeUser(data, condo){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/condos/' + condo + '/authorize/', data, { headers: this.authProvider.appendHeaders()})
      .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
      });
    });
  }

  updateUserProfile(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
        this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail , data, { headers: this.authProvider.appendHeaders()})
          .subscribe( data => {
            resolve(data);
          }, (err) => {
            reject(err);
          });
      });
  }

  updateUser(data) {
   return new Promise ( (resolve, reject) => {
    this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/'+ this.authProvider.userEmail , data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserActivities(data, activity){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/activities/'+ activity, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserAddresses(data, address){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/addresses/'+ address, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserCondos(data, condo){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/condos/' + condo, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserFamilies(data, family){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/families/' + family, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserGymCards(data, gymCard){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/gymcards/' + gymCard, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserHealthTips(data, healthTip){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/healthtips/' + healthTip, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserPrograms(data, program){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/programs/' + program, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserPromotions(data, promotion){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();

      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/promotions/' + promotion, data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateUserRole(user, data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.put(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/roles/' + user.email + '/changes/' , data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }


  // USERS REST DELETE
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  deleteUser(data) {
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserActivities(activity){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/activities/', { headers: this.authProvider.appendHeaders(),params: { activity }})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserAddresses(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/addresses/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserCondos(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/condos/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserEvents(event){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/events/', { headers: this.authProvider.appendHeaders(),params: { event }})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserFamilies(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/families/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserGymCards(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/gymcards/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserHealthTips(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/healthtips/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserPrograms(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/programs/', { headers: this.authProvider.appendHeaders(), params: { data }})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserPromotions(data){
    return new Promise ( (resolve, reject) => {
      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/promotions/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteUserRoles(data){
    return new Promise ( (resolve, reject) => {

      this.authProvider.appendContentType();
      this.http.delete(this.authProvider.apiUrl+'users/emails/'+ this.authProvider.userEmail + '/roles/'+ data, { headers: this.authProvider.appendHeaders()})
        .subscribe( data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

}
